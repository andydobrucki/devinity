var gulp = require('gulp');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');

gulp.task('header.css', function () {
  var scripts = [
    'plugins/boostrapv3/css/bootstrap.min.css',
    'plugins/font-awesome/css/font-awesome.css',
    'plugins/jquery-scrollbar/jquery.scrollbar.css',
    'plugins/switchery/css/switchery.min.css',
    'css/animate.css',
    'css/pages-icons.css',
    'css/pages.css',
    'css/css-view/main.css',
    'css/search.css',
    'css/dashboard.css',
    'plugins/pace/pace.min.js',
    'plugins/pace/pace-theme-flash.css'
  ];
  scripts = scripts.map(function (path) {
    return './static/' + path
  });
  return gulp.src(scripts)
    .pipe(concat('main.min.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest('./static/bundles/'))
})

gulp.task('devinity.css', function () {
  return gulp.src([
    './static/js/angular-toastr/angular-toastr.min.css',
    './static/js/ngTagsInput/ng-tags-input.min.css',
    './static/js/ngTagsInput/ng-tags-input.bootstrap.min.css',
    './static/js/angular-xeditable/css/xeditable.css',
    './static/js/select2/select2-master/select2.min.css',
    './static/plugins/bootstrap-select2/select2-bootstrap.css',
    './static/js/ui-select/select.min.css',
    './static/js/angular-feedback/angular-send-feedback.css',
    './static/js/ui-select/select.min.css',
    './node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.css',
  ]).pipe(concat('devinity.min.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest('./static/bundles/'))
});

gulp.task('devinity.libs', function () {
  return gulp.src([
    './static/js/lodash.min.js',
    './node_modules/js-cookie/src/js.cookie.js',
    './static/js/angular/angular.min.js',
    './static/js/angular/angular-animate.min.js',
    './static/js/angular/angular-sanitize.min.js',
    './static/js/angular/angular-cookies.min.js',
    './static/js/restangular/restangular.min.js',
    './static/js/ui-select/select.js',
    './static/js/angular-xeditable/js/xeditable.min.js',
    './static/js/angular-ui/ui-bootstrap-tpls-0.14.3.min.js',
    './static/js/angular-toastr/angular-toastr.tpls.min.js',
    './static/js/ngTagsInput/ng-tags-input.min.js',
    './static/js/wr-angular-image.min.js',
    './static/js/angular-feedback/angular-send-feedback.js',
    './static/js/ng-infinite-scroll.min.js',
    './static/js/ng-flow-standalone.min.js',
    './node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.js',
    './node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.js'
  ]).pipe(concat('devinity.libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./static/bundles/'))
});

gulp.task('default', ['devinity.css', 'devinity.libs']);
