CREATE TABLE users_languages_by_commits AS

select u.id, p.language, count(p.language) as language_count, u.login as user_login
	from users u 
	inner join commits c 
	on u.id = c.author_id
    join projects p
    on c.project_id = p.id
    group by u.id, p.language;