select u.login, pt.tag_name, count(pt.tag_name) as tag_count 
	from pull_request_comments_tags pt
	inner join pull_request_comments pc
	on pt.pull_request_id = pc.pull_request_id
    join users u
    on pc.user_id = u.id
    group by u.login, pt.tag_name
;