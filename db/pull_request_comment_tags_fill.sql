drop procedure if exists pull_request_comments_tags_fill;

delimiter #
create procedure pull_request_comments_tags_fill()
begin

	DECLARE current_pull_id INT(11);
    DECLARE current_body VARCHAR(255) CHARACTER SET latin1;
    
    DECLARE loopDone TINYINT default 0;
    DECLARE tloopDone TINYINT default 0;
    
    DECLARE current_tag_id INT(10);
    DECLARE current_tag_name VARCHAR(45) CHARACTER SET latin1;
    
	DECLARE transformed_body VARCHAR(255) CHARACTER SET latin1;
    DECLARE transformed_tag_name VARCHAR(45) CHARACTER SET latin1;
    
    DECLARE debug_enabled TINYINT default 0;
    SET @debug_enabled = FALSE;

	SET loopDone = 0;
	pull_cursor: BEGIN
		DECLARE pull_curs CURSOR FOR SELECT `pull_request_id`, CONVERT(`body` using 'latin1') FROM `github`.`pull_request_comments`;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET loopDone = 1;
        OPEN pull_curs;
		call debug_msg(@debug_enabled, "Cursor opened for pull_requests");
		REPEAT
			FETCH pull_curs INTO current_pull_id, current_body;
				IF NOT loopDone THEN
					-- call debug_msg(@debug_enabled, CONCAT("Just fetched a pull request: ", current_pull_id));
                    
                    SET tloopDone = 0;
                    tag_cursor: begin
						DECLARE tag_curs CURSOR FOR SELECT `id`,CONVERT(`tagname` using 'latin1') FROM `stack`.`tags`;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET tloopDone = 1;
						OPEN tag_curs;
                        REPEAT
							FETCH tag_curs INTO current_tag_id, current_tag_name;
                            IF NOT tloopDone THEN
								-- try to match
								set transformed_body = replace(current_body, '+', 'p');
                                set transformed_tag_name = replace(current_tag_name, '+', 'p');
                                set transformed_body = replace(transformed_body, '#', 'h');
                                set transformed_tag_name = replace(transformed_tag_name, '#', 'h');
                                set transformed_body = replace(transformed_body, '-', 'm');
                                set transformed_tag_name = replace(transformed_tag_name, '-', 'm');
                                set transformed_body = replace(transformed_body, '_', 'd');
                                set transformed_tag_name = replace(transformed_tag_name, '_', 'd');
                                set transformed_body = replace(transformed_body, '.', 't');
                                set transformed_tag_name = replace(transformed_tag_name, '.', 't');
                                
                                IF transformed_body REGEXP CONCAT(CONCAT('[[:<:]]',transformed_tag_name),'[[:>:]]') THEN
									
                                    set @export_stmt := 'INSERT INTO `github`.`pull_request_comments_tags` VALUES (__PULL_ID, __TAG_ID, \'__TAG_NAME\')';
									set @export_stmt := REPLACE(@export_stmt, '__PULL_ID', current_pull_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_ID', current_tag_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_NAME', current_tag_name);
									Prepare stmt FROM @export_stmt;
									Execute stmt;
									DEALLOCATE PREPARE stmt;
                                    
                                END IF;
                            END IF;
						UNTIL tloopDone END REPEAT;
                    end tag_cursor;
                    
				ELSE
					call debug_msg(@debug_enabled, "Loop done");
				END IF;
		UNTIL loopDone END REPEAT;
		CLOSE pull_curs;
	END pull_cursor;

end #
delimiter ;

call pull_request_comments_tags_fill();