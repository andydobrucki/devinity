drop procedure if exists commit_comments_tags_fill;

delimiter #
create procedure commit_comments_tags_fill()
begin

	DECLARE current_commit_id INT(11);
    DECLARE current_body VARCHAR(255) CHARACTER SET latin1;
    
    DECLARE loopDone TINYINT default 0;
    DECLARE tloopDone TINYINT default 0;
    
    DECLARE current_tag_id INT(10);
    DECLARE current_tag_name VARCHAR(45) CHARACTER SET latin1;
    
	DECLARE transformed_body VARCHAR(255) CHARACTER SET latin1;
    DECLARE transformed_tag_name VARCHAR(45) CHARACTER SET latin1;
    
    DECLARE debug_enabled TINYINT default 0;
    SET @debug_enabled = FALSE;

	SET loopDone = 0;
	commit_cursor: BEGIN
		DECLARE commit_curs CURSOR FOR SELECT `id`,CONVERT(`body` using 'latin1') FROM `github`.`commit_comments`;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET loopDone = 1;
        OPEN commit_curs;
		call debug_msg(@debug_enabled, "Cursor opened for commits");
		REPEAT
			FETCH commit_curs INTO current_commit_id, current_body;
				IF NOT loopDone THEN
					-- call debug_msg(@debug_enabled, CONCAT("Just fetched a commit: ", current_commit_id));
                    
                    SET tloopDone = 0;
                    tag_cursor: begin
						DECLARE tag_curs CURSOR FOR SELECT `id`,CONVERT(`tagname` using 'latin1') FROM `stack`.`tags`;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET tloopDone = 1;
						OPEN tag_curs;
                        REPEAT
							FETCH tag_curs INTO current_tag_id, current_tag_name;
                            IF NOT tloopDone THEN
								-- try to match
								set transformed_body = replace(current_body, '+', 'p');
                                set transformed_tag_name = replace(current_tag_name, '+', 'p');
                                set transformed_body = replace(transformed_body, '#', 'h');
                                set transformed_tag_name = replace(transformed_tag_name, '#', 'h');
                                set transformed_body = replace(transformed_body, '-', 'm');
                                set transformed_tag_name = replace(transformed_tag_name, '-', 'm');
                                set transformed_body = replace(transformed_body, '_', 'd');
                                set transformed_tag_name = replace(transformed_tag_name, '_', 'd');
                                set transformed_body = replace(transformed_body, '.', 't');
                                set transformed_tag_name = replace(transformed_tag_name, '.', 't');
                                
                                IF transformed_body REGEXP CONCAT(CONCAT('[[:<:]]',transformed_tag_name),'[[:>:]]') THEN
									
                                    set @export_stmt := 'INSERT INTO `github`.`commit_comments_tags` VALUES (__COMMIT_ID, __TAG_ID, \'__TAG_NAME\')';
									set @export_stmt := REPLACE(@export_stmt, '__COMMIT_ID', current_commit_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_ID', current_tag_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_NAME', current_tag_name);
									Prepare stmt FROM @export_stmt;
									Execute stmt;
									DEALLOCATE PREPARE stmt;
                                    
                                END IF;
                            END IF;
						UNTIL tloopDone END REPEAT;
                    end tag_cursor;
                    
				ELSE
					call debug_msg(@debug_enabled, "Loop done");
				END IF;
		UNTIL loopDone END REPEAT;
		CLOSE commit_curs;
	END commit_cursor;

end #
delimiter ;

call commit_comments_tags_fill();