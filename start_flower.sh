#!/bin/bash

source /opt/ec2/devinityrubi.sh

flower --broker=$RUBI_BROKER_URL --basic_auth=$RUBI_BASIC_AUTH --port=$RUBI_PORT --log_file_prefix=$LOGFLOWER &

echo "Flower started"
