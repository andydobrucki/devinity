def intersect_lists(a, b):
	return set(a) & set(b)


def difference_lists(a, b):
	return set(a).difference(set(b))


def filter_user_skills(skills, user, filter_types=None):
	if filter_types is None:
		return filter(lambda x: x.user == user, skills)
	else:
		return filter(lambda x: (x.user == user) and (x.familiar in filter_types), skills)


def filter_user_programming_skills(skills, user, filter_types=None):
	if filter_types is None:
		return filter(lambda x: (x.user == user) and (x.tag.category.id == 3), skills)
	else:
		return filter(lambda x: (x.user == user) and (x.tag.category.id == 3) and (x.familiar in filter_types), skills)


def count_user_skills(skills, user, filter_types=None):
	return len(filter_user_skills(skills, user, filter_types))


def intersect_tags(s, left, right, allowed_skill_types=None):
	return intersect_lists([r.tag for r in filter_user_skills(s, left, allowed_skill_types)],
					 	   [r.tag for r in filter_user_skills(s, right, allowed_skill_types)])


def different_tags(s, left, right, allowed_skill_types=None):
	return difference_lists([r.tag for r in filter_user_skills(s, left, allowed_skill_types)],
			 	    [r.tag for r in filter_user_skills(s, right, allowed_skill_types)])


def mentoring_relation_tags(s, left, right, left_skill_types, right_skill_types):
	return intersect_lists([r.tag for r in filter_user_skills(s, left, left_skill_types)],
					 	   [r.tag for r in filter_user_skills(s, right, right_skill_types)])


def result_json(time, count_pairs, rubicore_version):
	return {'time': time, 'pairs': count_pairs, 'version': rubicore_version}
