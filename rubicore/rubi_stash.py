from Queue import LifoQueue, Queue
import time

jobs = dict()  # dictionaries are mutable

def put_job(key):
	if key in jobs:
		queue = jobs[key]
		queue.put(time.time())
	else:
		queue = LifoQueue(maxsize=1)
		queue.put(time.time())
		jobs[key] = queue

def get_job_if_any(key):
	if key in jobs:
		try:
			return jobs[key].get()
		except Queue.Empty:
			return None
