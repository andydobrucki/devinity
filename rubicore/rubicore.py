import itertools
from celery.task import task
from django.db import transaction
import rubicore_tools as rb
import time
import devinity_connector as apps
import transactional_connector as rubi_transactional
# import rubi_stash as stash
# from celery.utils.log import get_task_logger
# from celery_once import QueueOnce, AlreadyQueued
import rubicore_settings as mechanism
from rubicore_settings import RubicoreVersion
from fudgesicle.fudgesicle import Fudgesicle
from decimal import Decimal

__version__ = '1.1'  # Rubicore "first version"


@task(name='rubicore.integrity_watcher', ignore_result=True)
def integrity_watcher(blacklisted_organizations):
    organization_model = apps.load_organizations_from_registry()
    existing = organization_model.objects
    for blacklisted in blacklisted_organizations:
        existing = existing.exclude(id=blacklisted)
    for organization in existing:
        full_launcher.delay(organization.id)


@task(name='rubicore.full_launcher', ignore_result=False)
def full_launcher(organization_instance):
    # method reserved for celery beat only, currently with redis as a broker
    # at the moment, runs for around 45 sec. and occupies around 25% processor power
    # another method for running full-rubicore is visiting
    # http://address:port/api/rubicore/execute
    if mechanism.get_version(return_enum=True) == RubicoreVersion.rubi:
        return Rubicore(organization=organization_instance).intelligence()
    elif mechanism.get_version(return_enum=True) == RubicoreVersion.fudgesicle:
        return Fudgesicle(organization=organization_instance).intelligence()
    else:
        raise NotImplementedError


@task(name='rubicore.launcher')  # base=QueueOnce
def launcher(organization_instance, filter_by_user=None):
    if mechanism.get_version(return_enum=True) == RubicoreVersion.rubi:
        return Rubicore(organization=organization_instance).intelligence(filter_by_user)
    elif mechanism.get_version(return_enum=True) == RubicoreVersion.fudgesicle:
        return Fudgesicle(organization=organization_instance).intelligence(filter_by_user)
    else:
        raise NotImplementedError
    # added = visit_stash((organization_instance, filter_by_user))
    # if added is not None:
    # 	result['stashed'] = added
    # return result


def apply_hard_rules(skill_instance, shuhari):
    SkillModel, = apps.load_from_registry(classes=['Skill'])
    user = skill_instance.user
    tag = skill_instance.tag

    if skill_instance.familiar == SkillModel.PASSIONATE:
        existing = SkillModel.objects.filter(user=user, tag=tag, familiar=SkillModel.KNOWS)
        if existing.exists():
            existing = existing.first()
            if existing.level is None:
                existing.level = shuhari.Ri.level
            if existing.level >= shuhari.Ri.level:
                pass
            else:
                existing.level += Decimal(shuhari.advance) * existing.level
        else:
            # is passionate, but not in skillbox
            skill_instance.level = shuhari.Ri.level
    return skill_instance


# def stash_for_later(key):
# 	stash.put_job(key)
#
#
# def visit_stash(key):
# 	smark = stash.get_job_if_any(key)
# 	print "visit 1"
# 	if smark is not None:
# 		try:
# 			print "visit 2"
# 			launcher.delay(*key)
# 			print "visit 3"
# 			return smark
# 		except AlreadyQueued:
# 			print "AlreadyQueued"
# 			pass  # already outdated by a new celery task
# 	return None

class Rubicore():
    def __init__(self, organization=None):
        self.organization = organization

    def collect(self, skills):
        dataset = skills.objects
        if self.organization is None:
            return dataset
        if type(self.organization) == int:
            return dataset.filter(user__organization__id=self.organization)
        else:
            return dataset.filter(user__organization=self.organization)

    def calculate_homophily(self, shared_tags, skills, user, filter_type):
        return float(len(shared_tags)) / rb.count_user_skills(skills, user, filter_type)

    def calculate_heterophily(self, different_tags, skills, user, filter_type):
        return float(len(different_tags)) / rb.count_user_skills(skills, user, filter_type)

    def intelligence(self, filter_by_user=None):
        print "Starting Rubicore" + (" full run" if filter_by_user is None else "")

        self.start = time.clock()

        SkillModel, MatchModel, MatchReasonModel = apps.load_from_registry()

        if (filter_by_user is not None) and (type(filter_by_user) == int):
            filter_by_user = apps.load_users_from_registry().objects.get(id=filter_by_user)

        self.mastership_type = (SkillModel.KNOWS, SkillModel.PASSIONATE)
        self.student_type = (SkillModel.WANT_TO_LEARN,)

        self.queryset = self.collect(SkillModel)
        self.skills = self.queryset.all()
        self.users = set([skill.user for skill in self.skills])
        self.pairs = itertools.combinations(self.users, 2)
        self.processed = 0

        for pair in self.pairs:

            if (filter_by_user is not None) and (filter_by_user not in pair):
                continue

            left_user, right_user = pair

            pair_shared_tags = rb.intersect_tags(self.skills, left_user, right_user,
                                                 allowed_skill_types=self.mastership_type)
            # allowed_skill_types in rb.intersect_tags solves bug #110671128
            pair_different_tags = rb.mentoring_relation_tags(self.skills, left_user, right_user,
                                                             left_skill_types=self.mastership_type,
                                                             right_skill_types=self.student_type)
            # left learns from right (right teaches left)
            pair_different_tags_reversed = rb.mentoring_relation_tags(self.skills, right_user, left_user,
                                                                      left_skill_types=self.student_type,
                                                                      right_skill_types=self.mastership_type)
            # right learns from left (left teaches right)
            # print pair_different_tags == pair_different_tags_reversed

            try:
                homophily_strength = self.calculate_homophily(pair_shared_tags, self.skills,
                                                              left_user,
                                                              self.mastership_type)  # Is always between 0.0 and 1.0
            except ZeroDivisionError:
                homophily_strength = 0.0
            try:
                heterophily_strength = self.calculate_heterophily(pair_different_tags, self.skills,
                                                                  right_user,
                                                                  self.student_type)  # Is always between 0.0 and 1.0 # podmiene to!!!!!!
            except ZeroDivisionError:
                heterophily_strength = 0.0  # I'm not sure about this, could be 1.0

            match_reason = MatchReasonModel.objects.create()
            for tag in pair_shared_tags:
                match_reason.shared_skills.add(tag)
            for tag in pair_different_tags:
                match_reason.left_teaches_right.add(tag)
            for tag in pair_different_tags_reversed:
                match_reason.right_teaches_left.add(tag)
            match_reason.save()

            with transaction.atomic():
                rubi_transactional.create_or_update_match(MatchModel, left_user, right_user, homophily_strength, heterophily_strength, match_reason)

            pair_different_tags = rb.mentoring_relation_tags(self.skills, right_user, left_user,
                                                             left_skill_types=self.mastership_type,
                                                             right_skill_types=self.student_type)
            pair_different_tags_reversed = rb.mentoring_relation_tags(self.skills, left_user, right_user,
                                                                      left_skill_types=self.student_type,
                                                                      right_skill_types=self.mastership_type)

            try:
                homophily_strength = self.calculate_homophily(pair_shared_tags, self.skills, right_user,
                                                              self.mastership_type)
            except ZeroDivisionError:
                homophily_strength = 0.0
            try:
                heterophily_strength = self.calculate_heterophily(pair_different_tags, self.skills, left_user,
                                                                  self.student_type)
            except ZeroDivisionError:
                heterophily_strength = 0.0

            mirror_match_reason = MatchReasonModel.objects.create()
            for tag in pair_shared_tags:
                mirror_match_reason.shared_skills.add(tag)
            for tag in pair_different_tags:
                mirror_match_reason.left_teaches_right.add(tag)
            for tag in pair_different_tags_reversed:
                mirror_match_reason.right_teaches_left.add(tag)
            mirror_match_reason.save()

            with transaction.atomic():
                rubi_transactional.create_or_update_match(MatchModel, right_user, left_user, homophily_strength, heterophily_strength, mirror_match_reason)

            self.processed += 1

        self.end = time.clock()
        print 'Rubicore execution time was {0:.4f} seconds'.format(self.end - self.start)

        return rb.result_json('{0:.5f} seconds'.format(self.end - self.start), self.processed, __version__)
