def create_or_update_match(Match, user_1, user_2, homophily_strength, heterophily_strength, match_reason):
    try:
        existing_match = Match.objects.get(user_1=user_1, user_2=user_2)
        existing_match.homophily_strength = homophily_strength
        existing_match.heterophily_strength = heterophily_strength
        existing_match.reason = match_reason
        existing_match.save()
    except Match.MultipleObjectsReturned:
        Match.objects.filter(user_1=user_1, user_2=user_2).delete()
        create_match(user_1, user_2, homophily_strength, heterophily_strength, match_reason)
    except Match.DoesNotExist:
        create_match(user_1, user_2, homophily_strength, heterophily_strength, match_reason)


def create_match(Match, user_1, user_2, homophily_strength, heterophily_strength, match_reason):
    Match.objects.create(user_1=user_1, user_2=user_2,
                         homophily_strength=homophily_strength,
                         heterophily_strength=heterophily_strength,
                         reason=match_reason)
