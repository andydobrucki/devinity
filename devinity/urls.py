from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import logout, login
from rest_framework.urlpatterns import format_suffix_patterns
from devinityapp import views
from devinityapp.views import Index, Dashboard, Signup, SearchView, \
    FeedbackListView, UserDetailView, OrgCodeFormView, Onboarding, \
    Signed, MobileView
from api_urls import api_router

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', Index.as_view(), name='index'),
    url(r'^(?P<user_id>\d+)/$', UserDetailView.as_view(), name='index-profile'),
    url(r'^dashboard/', Dashboard.as_view(), name='dashboard'),
    url(r'^timeline/', views.timeline, name='timeline'),
    url(r'^login/$', login, name='login_main'),
    url(r'^logout/$', logout, {'next_page': '/login/'}, name='logout_main'),
    url(r'^signup/', Signup.as_view(), name='signup'),
    url(r'^signed/', Signed.as_view(), name='signed'),
    url(r'^orgcodeform/', OrgCodeFormView.as_view(), name='orgcodeform'),
    url(r'^onboarding/', Onboarding.as_view(), name='onboarding'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^search/', SearchView, name='search'),
    url(r'^', include(api_router.urls)),
    url(r'^feedbacks/', FeedbackListView.as_view(), name='feedbacks'),
    url(r'^mobile/', MobileView.as_view(), name='mobile')


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)
