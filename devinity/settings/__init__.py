from devinity.settings.base import *

from devinity.settings.production import *

try:
    from devinity.settings.local import *
except:
    pass