from mixpanel import Mixpanel
from devinity.settings import MIXPANEL_KEY
from devinityapp.models import Skill


mp = None

if MIXPANEL_KEY is not None:
    mp = Mixpanel(MIXPANEL_KEY)


def mxp_report_skill(request):
    if mp is not None:
        payload = {'tag': request.data['tag'],
                   'org': request.user.organization.id}
        if request.data['familiar'] == Skill.WANT_TO_LEARN:
            mp.track(request.user.id, 'Create Aim', payload)
        elif request.data['familiar'] == Skill.KNOWS:
            mp.track(request.user.id, 'Create Skill', payload)
        elif request.data['familiar'] == Skill.PASSIONATE:
            mp.track(request.user.id, 'Create Passion', payload)


def mxp_report_flagset(request):
    if mp is not None:
        payload = {'flag': request.data['flag'],
                   'org': request.user.organization.id}
        mp.track(request.user.id, 'Create Flag', payload)


def mxp_report_exploration(request, foreign_id):
    if mp is not None:
        payload = {'foreign_id': foreign_id,
                   'org': request.user.organization.id}
        mp.track(request.user.id, 'Exploration', payload)


def mxp_report_display(request):
    if mp is not None:
        payload = {'org': request.user.organization.id}
        mp.track(request.user.id, 'MainPage Display', payload)


def mxp_report_dashboard(request):
    if mp is not None:
        payload = {'org': request.user.organization.id}
        mp.track(request.user.id, 'Dashboard Display', payload)