def similar_result_english_phrase(taglist):
    length = len(taglist)
    if length > 3:
        return "users share: %s and %r other" % (", ".join(taglist[:3]), length-3)
    elif length == 1:
        return "users share: %s" % taglist[0]
    elif length == 0:
        return "users don't share skills"
    return "users share skills %s" % ",".join(taglist)


def translate_similar_result(tagset, ling=False):
    taglist = [tag.name for tag in tagset.all()]
    return similar_result_english_phrase(taglist) if ling else taglist


def student_result_english_phrase(taglist):
    length = len(taglist)
    if length > 3:
        return "user can learn: %s and %r other" % (", ".join(taglist[:3]), length-3)
    elif length == 1:
        return "user can learn: %s" % taglist[0]
    elif length == 0:
        return "user can't learn new skills"
    return "user can learn: %s" % ",".join(taglist)


def translate_student_result(tagset, ling=False):
    taglist = [tag.name for tag in tagset.all()]
    return student_result_english_phrase(taglist) if ling else taglist


def teacher_result_english_phrase(taglist):
    length = len(taglist)
    if length > 3:
        return "user can teach: %s and %r other" % (", ".join(taglist[:3]), length-3)
    elif length == 1:
        return "user can teach: %s" % taglist[0]
    elif length == 0:
        return "user can't teach skills"
    return "user can teach skills %s" % ",".join(taglist)


def translate_teacher_result(tagset, ling=False):
    taglist = [tag.name for tag in tagset.all()]
    return teacher_result_english_phrase(taglist) if ling else taglist


def unicodetoint(instance):
    if type(instance) == unicode:
        try:
            return int(instance)
        except ValueError:
            pass
    else:
        return instance


class AlterItems(object):

    def __init__(self, queryset, **kwargs):
        self.queryset = queryset
        self.kwargs = kwargs

    def _clone(self):
        return AlterItems(queryset._clone(), **self.kwargs)

    def __iter__(self):
        for obj in self.queryset:
            for key, val in self.kwargs.items():
                setattr(obj, key, val)
            yield obj
