from devinityapp.mobileesp import mdetect
from django.http.response import HttpResponseRedirect

class MobileDetectionMiddleware(object):
    """
    Useful middleware to detect if the user is opening site
    on a mobile device.
    Based on:
    https://github.com/ahand/mobileesp
    inspired by:
    http://stackoverflow.com/questions/9119594/detect-mobile-tablet-or-desktop-on-django
    http://blog.mobileesp.com/?p=265
    """

    def process_request(self, request):
        if request.path == '/mobile/':
            return None

        is_phone = False

        user_agent = request.META.get("HTTP_USER_AGENT")
        http_accept = request.META.get("HTTP_ACCEPT")
        if user_agent and http_accept:
            agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
            is_phone = agent.detectTierIphone() or agent.detectMobileQuick()

        if is_phone:
            return HttpResponseRedirect('/mobile/')
        else:
            return None


