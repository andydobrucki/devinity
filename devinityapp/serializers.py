# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.fields import ReadOnlyField, SerializerMethodField, DateTimeField

from models import User, Skill, Flag, FlagSet, \
    Organization, Tag, TagWiki, Match, MatchReason, \
    TagCategory, MatchRejection  # PEP-0328


class UserSerializer(serializers.ModelSerializer):
    url = ReadOnlyField(source='get_absolute_url')
    avatar = SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'login', 'name', 'email', 'organization', 'position',
                  'joined', 'bio', 'motto', 'gender', 'location', 'country',
                  'social_so', 'social_gh', 'url', 'avatar', 'avatar_thumbnail')

    def get_avatar(self, obj):
        return obj.avatar.url if obj.avatar else None


class AvatarSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'id', 'avatar', 'avatar_thumbnail')


class FlagSerializer(serializers.ModelSerializer):
    count = ReadOnlyField(source='flagset_set.count')

    class Meta:
        model = Flag
        fields = ('id', 'category', 'name', 'count', 'description')


class FlagSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlagSet
        fields = ('id', 'user', 'flag')


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name', 'description')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'count')


class TagWikiSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagWiki
        fields = ('id', 'tag', 'description')


class TagCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TagCategory
        fields = ('id', 'description')


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('id', 'user', 'tag', 'familiar', 'level', 'importance',
                  'count')


class MatchReasonSerializer(serializers.ModelSerializer):
    shared_skills = TagSerializer(many=True, required=True)
    description = ReadOnlyField()
    left_teaches_right = TagSerializer(many=True, required=False)
    right_teaches_left = TagSerializer(many=True, required=False)

    class Meta:
        model = MatchReason
        #fields = ('id', 'shared_skills', 'left_teaches_right', 'right_teaches_left')
        fields = '__all__'


class MatchSerializer(serializers.ModelSerializer):
    url = ReadOnlyField(source='user_2.get_absolute_url')
    reason = MatchReasonSerializer(many=False, required=True)
    match_type = ReadOnlyField()
    user_1 = UserSerializer(read_only=True)
    user_2 = UserSerializer(read_only=True)

    class Meta:
        model = Match
        fields = '__all__'


class MatchRejectionSerializer(serializers.ModelSerializer):
    match = MatchSerializer(read_only=True)
    submitter = UserSerializer(read_only=True)
    timestamp = DateTimeField()

    class Meta:
        model = MatchRejection
        fields = '__all__'
