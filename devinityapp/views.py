import json
from allauth.socialaccount.models import SocialAccount
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.forms import modelform_factory
from django.http import Http404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from devinityapp import quote
from devinityapp.tracking_utils import mxp_report_exploration, mxp_report_display, mxp_report_dashboard
from models import User, Feedback, Organization
from django.views.generic import TemplateView, ListView, FormView, View


def loginwith(request):
    context = 0
    return render(request, 'loginwith.html', context)


class Signup(FormView):
    template_name = 'registration/signup.html'
    form_class = modelform_factory(
        User, fields=('username', 'password', 'email'))
    success_url = '/'

    def form_valid(self, form):
        form.save()


class Signed(View):

    def get(self, request, *args, **kwargs):
        if request.user.organization is None:
            return OrgCodeFormView.as_view()(request)
        else:
            return redirect('index')


class OrgCodeFormView(FormView):
    template_name = 'registration/orgcodeform.html'
    form_class = modelform_factory(User, fields=('username', 'password', 'email'))
    success_url = '/'

    def form_valid(self, form):
        form.save()


class Onboarding(View):

    def post(self, request, *args, **kwargs):
        provided_org_code = request.POST.get('org_code')
        o = Organization()  # nasty workaround, to skip implementing new Manager
        org_match = o.matching(provided_org_code)
        if org_match is None:
            return redirect('signed')
        # otherwise
        user = request.user
        user.organization, user.is_employee = org_match
        social_acc = SocialAccount.objects.get(user=user)
        user.name = social_acc.extra_data['name']
        user.motto = quote.random_quote()
        user.save()
        return redirect('index')


class SearchView(LoginRequiredMixin, TemplateView):
    template_name = 'search.html'


search = SearchView.as_view()


def timeline(request):
    context = 0
    return render(request, 'timeline.html', context)


def post_feedback(request, *args, **kwargs):
    feedback_data = request.POST['feedback']
    f = Feedback(reporter=request.user)
    status = f.propagate(feedback_data)
    f.save()
    name = request.POST.get('name')
    return HttpResponse(json.dumps({'name': name}),
                        content_type="application/json",
                        status=status)


class Index(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        if request.user.organization is None:
            return OrgCodeFormView.as_view()(request)

        mxp_report_display(request)

        user_id = self.kwargs.get('user_id', None)

        if user_id:
            try:
                user_organization = request.user.organization
                requested_organization = User.objects.get(id=user_id).organization
                if user_organization.id != requested_organization.id:
                    return HttpResponseRedirect('/')
            except User.DoesNotExist:
                return HttpResponseRedirect('/')

            mxp_report_exploration(request, user_id)
            if int(user_id) == request.user.id:
                return HttpResponseRedirect('/')
        return super(Index, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if 'feedback' in request.POST.keys():
            return post_feedback(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        user_id = self.kwargs.get('user_id', None)
        if user_id:
            context.update({'user_id': user_id})
        return context


class UserDetailView(Index):
    template_name = 'user_view.html'


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard.html'

    def post(self, request, *args, **kwargs):
        if 'feedback' in request.POST.keys():
            return post_feedback(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.organization is None:
            return OrgCodeFormView.as_view()(request)

        if request.user.is_manager:
            mxp_report_dashboard(request)
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)
        else:
            raise Http404('Not found')

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        user = self.request.user

        context.update({
            'mapped_developers_count': user.organization.count_staff,
            'mapped_technologies_count': user.organization.count_skills_uq
        })
        return context


class FeedbackListView(SuperuserRequiredMixin, ListView):
    model = Feedback
    template_name = 'feedbacks.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(FeedbackListView, self).get_context_data(**kwargs)

        for obj in context['object_list']:
            try:
                f = open(obj.html.path, 'r')
                obj.html_content = f.read()
                f.close()
            except IOError:
                obj.html_content = 'could not open file'

        return context


class MobileView(TemplateView):
    template_name = 'mobile.html'

