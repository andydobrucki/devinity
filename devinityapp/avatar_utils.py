from PIL import Image


def image_to_square(image):
    """
    This function is to crop any-size image to shorter edge sized square.
    The output region is always centered.
    :param image: input image
    :return: square-cropped part of image
    """
    size_x, size_y = image.size

    if size_x == size_y:
        return image

    #WARNING! Mathematic!
    elif size_x > size_y:
        crop_box = (int(round((size_x-size_y)/2)), 0,
                    int(round((size_x+size_y)/2)), size_y)
    elif size_x < size_y:
        crop_box = (0, int(round((size_y-size_x)/2)),
                    size_x, int(round((size_y+size_x)/2)))

    return image.crop(crop_box)


def image_downsize(image, size=500):
    """
    Scales down image if bigger than provided size
    :param image:
    :param size: target size of square image, never to be exceeded
    :return: scaled down image, or unchanged if smaller than specified size
    """
    size_x, size_y = image.size
    if size_x > size or size_y > size:
        return image.resize((size, size), Image.ANTIALIAS)
    else:
        return image


def process_uploaded_avatar(image_file, filename):
    img = Image.open(image_file.path)
    image_downsize(image_to_square(img))\
        .save(image_file.path, 'JPEG', quality=75)


def create_thumbnail(avatar, size=(100,100)):
    image = Image.open(avatar.path)
    image.resize(size, Image.ANTIALIAS)\
        .save(('%s.thumb' % avatar.path), 'JPEG', quality=75)
    return ('%s.thumb' % avatar)
