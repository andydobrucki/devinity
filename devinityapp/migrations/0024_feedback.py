# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0023_auto_20160123_0045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('browser_info', models.TextField()),
                ('url', models.CharField(max_length=200)),
                ('html', models.FileField(upload_to=b'')),
                ('screen_shot', models.ImageField(upload_to=b'')),
                ('note', models.TextField()),
            ],
        ),
    ]
