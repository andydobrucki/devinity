# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0007_auto_20151105_1817'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='has_synonyms',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='is_moderator_only',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='is_required',
        ),
        migrations.AddField(
            model_name='tag',
            name='count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='tag',
            name='name',
            field=models.CharField(default=None, max_length=50),
        ),
        migrations.AddField(
            model_name='tag',
            name='wiki',
            field=models.OneToOneField(default=None, to='devinityapp.TagWiki'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
