# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0013_match'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='familiar',
            field=models.PositiveSmallIntegerField(choices=[(0, b'Want to learn'), (1, b'Knows'), (2, b'Passionate')]),
        ),
    ]
