# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0028_auto_20160209_2333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='manager_key',
            field=models.CharField(default=None, max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='organization',
            name='user_key',
            field=models.CharField(default=None, max_length=20, null=True, blank=True),
        ),
    ]
