# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0015_auto_20151121_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='level',
            field=models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=10, choices=[(0.119202922, b'Know it'), (0.880797077, b'Can work with it'), (0.997527376, b'Can teach it')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='bio',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='country',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.SmallIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='is_employee',
            field=models.SmallIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='joined',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='location',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='motto',
            field=models.CharField(max_length=150, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='organization',
            field=models.ForeignKey(blank=True, to='devinityapp.Organization', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='position',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='sample',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='user',
            name='social_gh',
            field=models.ForeignKey(blank=True, to='devinityapp.Social_GH', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='social_so',
            field=models.ForeignKey(blank=True, to='devinityapp.Social_SO', null=True),
        ),
    ]
