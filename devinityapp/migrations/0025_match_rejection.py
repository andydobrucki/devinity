# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0024_feedback'),
    ]

    operations = [
        migrations.CreateModel(
            name='MatchRejection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('match', models.ForeignKey(default=None, to='devinityapp.Match')),
                ('submitter', models.ForeignKey(default=None, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='feedback',
            name='html',
            field=models.FileField(null=True, upload_to=b'feedback', blank=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='screen_shot',
            field=models.ImageField(null=True, upload_to=b'feedback', blank=True),
        ),
    ]
