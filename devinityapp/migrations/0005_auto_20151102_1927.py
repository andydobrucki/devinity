# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0004_auto_20151102_0130'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='password',
            field=models.CharField(default=datetime.datetime(2015, 11, 2, 19, 27, 32, 166227, tzinfo=utc), max_length=128, verbose_name='password'),
            preserve_default=False,
        ),
    ]
