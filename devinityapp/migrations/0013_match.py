# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0012_auto_20151113_2101'),
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('homophily_strength', models.DecimalField(max_digits=12, decimal_places=12)),
                ('heterophily_strength', models.DecimalField(max_digits=12, decimal_places=12)),
                ('user_1', models.ForeignKey(related_name='match_left_user', default=None, to=settings.AUTH_USER_MODEL)),
                ('user_2', models.ForeignKey(related_name='match_right_user', default=None, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
