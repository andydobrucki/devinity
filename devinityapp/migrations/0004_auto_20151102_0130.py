# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0003_flag_flagset'),
    ]

    operations = [
        migrations.RenameField(
            model_name='organization',
            old_name='manifesto',
            new_name='description',
        ),
    ]
