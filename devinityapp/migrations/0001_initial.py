# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ExpertProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Flag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.PositiveSmallIntegerField()),
                ('name', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='FlagSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('flag', models.ForeignKey(to='devinityapp.Flag')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('manifesto', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('holder', models.CharField(max_length=50)),
                ('tag_name', models.CharField(max_length=50)),
                ('familiar', models.SmallIntegerField()),
                ('level', models.DecimalField(max_digits=10, decimal_places=10)),
                ('importance', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Social_GH',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField()),
                ('company', models.CharField(max_length=50)),
                ('ext_ref_id', models.CharField(max_length=24)),
                ('type', models.CharField(max_length=50)),
                ('followers', models.IntegerField()),
                ('following', models.IntegerField()),
                ('num_owned_repos', models.IntegerField()),
                ('total_owned_repos_stars', models.IntegerField()),
                ('total_owned_repos_forks', models.IntegerField()),
                ('total_owned_repos_issues', models.IntegerField()),
                ('num_reported_issues', models.IntegerField()),
                ('num_assigned_issues', models.IntegerField()),
                ('total_owned_repos_open_issues', models.IntegerField()),
                ('total_owned_repos_pulls', models.IntegerField()),
                ('total_owned_repos_commits', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Social_SO',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('count', models.IntegerField()),
                ('has_synonyms', models.BooleanField()),
                ('is_moderator_only', models.BooleanField()),
                ('is_required', models.BooleanField()),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=100)),
                ('position', models.CharField(max_length=50)),
                ('joined', models.DateTimeField(auto_now_add=True)),
                ('bio', models.CharField(max_length=200)),
                ('motto', models.CharField(max_length=150)),
                ('sample', models.BooleanField()),
                ('gender', models.SmallIntegerField()),
                ('location', models.CharField(max_length=50)),
                ('country', models.CharField(max_length=50)),
                ('is_employee', models.SmallIntegerField()),
                ('organization', models.ForeignKey(to='devinityapp.Organization')),
                ('social_gh', models.ForeignKey(to='devinityapp.Social_GH')),
                ('social_so', models.ForeignKey(to='devinityapp.Social_SO')),
            ],
        ),
        migrations.AddField(
            model_name='flagset',
            name='user',
            field=models.ForeignKey(to='devinityapp.User'),
        ),
        migrations.AddField(
            model_name='expertprofile',
            name='skills',
            field=models.ManyToManyField(to='devinityapp.Skill'),
        ),
        migrations.AddField(
            model_name='expertprofile',
            name='user',
            field=models.ForeignKey(to='devinityapp.User'),
        ),
    ]
