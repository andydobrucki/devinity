# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0017_auto_20151130_2210'),
    ]

    operations = [
        migrations.CreateModel(
            name='MatchReason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('left_teaches_right', models.ManyToManyField(default=None, related_name='matchreason_left_mastery', to='devinityapp.Skill', blank=True)),
                ('right_teaches_left', models.ManyToManyField(default=None, related_name='matchreason_right_mastery', to='devinityapp.Skill', blank=True)),
                ('shared_skills', models.ManyToManyField(default=None, to='devinityapp.Skill')),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='reasons',
            field=models.ForeignKey(default=None, to='devinityapp.MatchReason'),
        ),
    ]
