# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0025_match_rejection'),
    ]

    operations = [
        migrations.AddField(
            model_name='flag',
            name='description',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
