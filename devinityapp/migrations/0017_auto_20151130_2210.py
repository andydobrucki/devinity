# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0016_auto_20151129_0013'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='avatar',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(location=b'/devinity_media/user_data/images/'), null=True, upload_to=b'avatar', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='is_org_admin',
            field=models.BooleanField(default=False),
        ),
    ]
