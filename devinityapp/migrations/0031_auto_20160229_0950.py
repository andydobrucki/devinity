# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0030_auto_20160214_2235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='is_employee',
            field=models.SmallIntegerField(blank=True, null=True, choices=[(1, b'Staff'), (2, b'Manager')]),
        ),
    ]
