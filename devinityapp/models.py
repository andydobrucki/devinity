import base64
import json
import datetime
import uuid
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import python_2_unicode_compatible
from rubicore import rubicore as rubi
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
# from threading import Thread
import constants as shuhari
from celery_once import AlreadyQueued


@python_2_unicode_compatible
class TagWiki(models.Model):
    description = models.TextField()

    def __str__(self):
        return u''


@python_2_unicode_compatible
class TagCategory(models.Model):
    description = models.CharField(max_length=40, blank=False, default=None)

    def __str__(self):
        return u''


@python_2_unicode_compatible
class Tag(models.Model):
    name = models.CharField(max_length=50, null=False, default=None)
    category = models.ForeignKey(TagCategory, null=True, blank=True,
                                 default=None)
    count = models.IntegerField(default=0)
    # how popular tag is on StackOverflow
    wiki = models.OneToOneField(TagWiki, default=None)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name or u''


@python_2_unicode_compatible
class Organization(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    user_key = models.CharField(max_length=20, default=None, null=True,
                                blank=True)
    manager_key = models.CharField(max_length=20, default=None, null=True,
                                   blank=True)

    def __str__(self):
        return self.name or u''

    def save(self, *args, **kwargs):
        if not self.user_key:
            self.user_key = self.generate_key()
        if not self.manager_key:
            self.manager_key = self.generate_key(different_from=self.user_key)
        super(Organization, self).save(*args, **kwargs)

    def generate_key(self, different_from=''):
        while True:
            key = base64.b64encode(str(uuid.uuid4()))[:16]
            if self.is_unique(key) and key != different_from:
                break
        return key

    def is_unique(self, key):
        for org in Organization.objects.all():
            if key == org.user_key or key == org.manager_key:
                return False
        return True

    def matching(self, key):
        for org in Organization.objects.all():
            if key == org.user_key:
                return org, User.STAFF
            if key == org.manager_key:
                return org, User.MANAGER
        return None

    @property
    def count_staff(self):
        """
        :return: number of users in this organization
        """
        return User.objects \
            .filter(organization=self) \
            .count()

    @property
    def count_skills(self):
        """
        :return: number of all skills mapped by users in this organization
        """
        return Skill.objects \
            .filter(user__organization=self) \
            .count()

    @property
    def count_skills_uq(self):
        """
        :return: number of unique skills of all users in organization
        """
        return Skill.objects \
            .filter(user__organization=self) \
            .filter(familiar=Skill.KNOWS) \
            .values('tag') \
            .distinct() \
            .count()


@python_2_unicode_compatible
class Social_SO(models.Model):
    login = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField()

    def __str__(self):
        return self.name or u''


@python_2_unicode_compatible
class Social_GH(models.Model):
    login = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField()
    company = models.CharField(max_length=50)
    ext_ref_id = models.CharField(max_length=24)
    type = models.CharField(max_length=50)
    followers = models.IntegerField()
    following = models.IntegerField()
    num_owned_repos = models.IntegerField()
    total_owned_repos_stars = models.IntegerField()
    total_owned_repos_forks = models.IntegerField()
    total_owned_repos_issues = models.IntegerField()
    num_reported_issues = models.IntegerField()
    num_assigned_issues = models.IntegerField()
    total_owned_repos_open_issues = models.IntegerField()
    total_owned_repos_pulls = models.IntegerField()
    total_owned_repos_commits = models.IntegerField()

    def __str__(self):
        return self.name or u''


@python_2_unicode_compatible
class User(AbstractUser):

    STAFF = 1
    MANAGER = 2

    USER_CAT_CHOICES = (
        (STAFF, 'Staff'),
        (MANAGER, 'Manager'),
    )
    login = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=50)
    organization = models.ForeignKey(Organization, blank=True, null=True)
    position = models.CharField(max_length=50, blank=True, null=True)
    joined = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    bio = models.CharField(max_length=200, blank=True, null=True)
    motto = models.CharField(max_length=150, blank=True, null=True)
    sample = models.BooleanField(default=False)
    gender = models.SmallIntegerField(blank=True, null=True)
    location = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    is_employee = models.SmallIntegerField(choices=USER_CAT_CHOICES, blank=True, null=True)
    is_org_admin = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to='avatar', blank=True, null=True)
    avatar_thumbnail = models.ImageField(
        upload_to='avatar', blank=True, null=True)
    social_so = models.ForeignKey(Social_SO, blank=True, null=True)
    social_gh = models.ForeignKey(Social_GH, blank=True, null=True)
    # other social accounts to be included later

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['name', 'email']

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.name

    def __str__(self):
        return self.username or u''

    @property
    def is_manager(self):
        return self.is_employee == 2

    def get_avatar_url(self):
        return self.avatar.url if self.avatar else '/media/avatar/placeholder.png'

    def get_absolute_url(self):
        return reverse('index-profile', kwargs={'user_id': self.id})


@python_2_unicode_compatible
class Skill(models.Model):
    WANT_TO_LEARN = 0
    KNOWS = 1
    PASSIONATE = 2

    FAMILIARITY_CHOICES = (
        (WANT_TO_LEARN, 'Want to learn'),
        (KNOWS, 'Knows'),
        (PASSIONATE, 'Passionate'),
    )

    KNOW = shuhari.Shu.level
    CAN_WORK_WITH = shuhari.Ha.level
    CAN_TEACH = shuhari.Ri.level

    LEVEL_CHOICES = (
        (KNOW, 'Know it'),
        (CAN_WORK_WITH, 'Can work with it'),
        (CAN_TEACH, 'Can teach it')
    )

    user = models.ForeignKey(User, default=None, null=True, blank=True)
    tag = models.ForeignKey(Tag, default=None, null=True, blank=True)
    familiar = models.PositiveSmallIntegerField(choices=FAMILIARITY_CHOICES)
    level = models.DecimalField(max_digits=10, decimal_places=10,
                                choices=LEVEL_CHOICES, null=True, blank=True)
    importance = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return (self.user.username + '_' + self.tag.name) or u''

    @property
    def count(self):
        """
        :return: number of users from same organization who have this skill
        in the same category
        """
        return Skill.objects \
            .filter(user__organization=self.user.organization) \
            .filter(tag=self.tag) \
            .filter(familiar=self.familiar) \
            .distinct() \
            .count()


@receiver(pre_save, sender=Skill, dispatch_uid="pre_update_skill")
def pre_update_skill(sender, instance, **kwargs):
    instance = rubi.apply_hard_rules(instance, shuhari)


@receiver(post_save, sender=Skill, dispatch_uid="update_skill")
def update_skill(sender, instance, **kwargs):
    try:
        rubi.launcher.delay(instance.user.organization.id, instance.user.id)
    except AlreadyQueued:
        pass
        # print str((instance.user.organization.id, instance.user.id)) + " is already queued!"
        # rubi.stash_for_later((instance.user.organization.id, instance.user.id))
    # Since Celery is a distributed system, you can't know in which process,
    # or even on what machine the task will run.
    # So you shouldn't pass Django model objects as arguments to tasks,
    # its almost always better to re-fetch the object from the database instead,
    # as there are possible race conditions involved.


@receiver(post_delete, sender=Skill, dispatch_uid="delete_skill")
def delete_skill(sender, instance, **kwargs):
    #thread = Thread(target = rubi.launcher, args = (instance.user.organization, instance.user))
    #thread.start()
    try:
        rubi.launcher.delay(instance.user.organization.id, instance.user.id)
    except AlreadyQueued:
        pass
        # print str((instance.user.organization.id, instance.user.id)) + " is already queued!"
        # rubi.stash_for_later((instance.user.organization.id, instance.user.id))


class ExpertProfile(models.Model):
    user = models.ForeignKey(User)
    skills = models.ManyToManyField(Skill)

    def __unicode__(self):
        return self.user or u''


@python_2_unicode_compatible
class Flag(models.Model):
    # categories
    OPPORTUNITY = 1
    INFORMATION = 2
    WARNING = 3

    CATEGORY_CHOICES = (
        (OPPORTUNITY, 'Opportunity'),
        (INFORMATION, 'Information'),
        (WARNING, 'Warning'),
    )

    category = models.PositiveSmallIntegerField(choices=CATEGORY_CHOICES)
    name = models.CharField(max_length=80)
    description = models.TextField()

    def get_count_for_organization(self, organization):
        return self.flagset_set.filter(user__organization=organization).count()

    def __str__(self):
        return self.name or ''


@python_2_unicode_compatible
class FlagSet(models.Model):
    user = models.ForeignKey(User)
    flag = models.ForeignKey(Flag)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (self.user.username + '_' + self.flag.name) or ''

    @property
    def count(self):
        """
        :return: number of users in the same organization
        who have this flag set
        """
        return FlagSet.objects \
            .filter(user__organization=self.user.organization) \
            .filter(flag=self.flag) \
            .distinct() \
            .count()


@python_2_unicode_compatible
class MatchReason(models.Model):
    shared_skills = models.ManyToManyField(Tag, default=None, null=False,
                                           blank=False)
    # This is controversial. But don't match heterophilicly when there are zero similar skills,
    # we don't want frontend developers to be matched with AI specialists and robohand programmers
    # but rather heterophily to be a way to support quite similar(!) people in their learning process
    left_teaches_right = models.ManyToManyField(
        Tag, default=None, null=True, blank=True,
        related_name='%(class)s_left_mastery'
    )
    right_teaches_left = models.ManyToManyField(
        Tag, default=None, null=True, blank=True,
        related_name='%(class)s_right_mastery'
    )
    # This must be association to Tag, not Skill. While Rubicore processes all skills,
    # match reasons are information strictly on Tags and result of set operations on them
    _description = None

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    def __str__(self):
        return self.id or u''


@python_2_unicode_compatible
class Match(models.Model):
    user_1 = models.ForeignKey(User, default=None,
                               null=False, blank=False,
                               related_name='%(class)s_left_user')
    user_2 = models.ForeignKey(User, default=None,
                               null=False, blank=False,
                               related_name='%(class)s_right_user')
    homophily_strength = models.DecimalField(max_digits=12,
                                             decimal_places=11)
    heterophily_strength = models.DecimalField(max_digits=12,
                                               decimal_places=11)
    _match_type = None

    reason = models.ForeignKey(MatchReason, default=None, null=False,
                               blank=False)

    @property
    def match_type(self):
        return self._match_type

    @match_type.setter
    def match_type(self, value):
        self._match_type = value

    def __str__(self):
        return (self.user_1.username + '_' + self.user_2.username) or u''


@python_2_unicode_compatible
class Feedback(models.Model):

    reporter = models.ForeignKey(User, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    browser_info = models.TextField()
    url = models.CharField(max_length=200)
    html = models.FileField(upload_to='feedback', blank=True, null=True)
    screen_shot = models.ImageField(upload_to='feedback', blank=True,
                                    null=True)
    note = models.TextField()

    class Meta:
        ordering = ['-timestamp']

    def propagate(self, json_str):
        try:
            json_data = json.loads(json_str)
            self.timestamp = datetime.datetime.now()

            if 'browser' in json_data.keys():
                self.browser_info = json.dumps(json_data['browser'])

            if 'url' in json_data.keys():
                self.url = json_data['url']

            if 'html' in json_data.keys():
                html_file = ContentFile(json_data['html'])
                html_file.name = '%s.html' % self.timestamp
                self.html = html_file

            if 'img' in json_data.keys():
                _, b64data = json_data['img'].split(',')
                png_file = ContentFile(base64.decodestring(b64data))
                png_file.name = '%s.png' % self.timestamp
                self.screen_shot = png_file

            if 'note' in json_data.keys():
                self.note = json_data['note']
        except:
            return 500
        else:
            return 201

    def __str__(self):
        return '%s_%s_%s' % (self.id, self.user, self.timestamp) or u''


@python_2_unicode_compatible
class MatchRejection(models.Model):
    # which match is reported as wrong
    match = models.ForeignKey(Match, default=None,
                              null=False, blank=False)
    # who reports the match as wrong
    submitter = models.ForeignKey(User, default=None, null=False, blank=False)
    # when was the match reported
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (self.match + '_' + self.submitter) or u''
