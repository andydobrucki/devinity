from rest_framework.pagination import PageNumberPagination


class SimplePaginator(PageNumberPagination):
    max_page_size = 10
    page_size = 10
