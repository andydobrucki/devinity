from django_filters import FilterSet, CharFilter, RangeFilter
from django_filters.filters import MethodFilter

from devinityapp.models import TagWiki, Flag, Tag, Match

__author__ = 'wojciech'


class TagWikiFilter(FilterSet):
    class Meta:
        model = TagWiki
        fields = ('tag',)


class FlagFilter(FilterSet):
    name = CharFilter(lookup_type='icontains')

    class Meta:
        model = Flag
        fields = ('name',)


class TagFilter(FilterSet):
    exclude = MethodFilter(action='filter_exclude')
    name = MethodFilter(action='filter_name')

    class Meta:
        model = Tag
        fields = ('name', 'exclude')

    def filter_exclude(self, qs, value):
        ids = self.data.getlist('exclude')
        return qs.exclude(id__in=ids)

    def filter_name(self, qs, value):
        if len(value) == 1:
            return qs.filter(name=value)
        elif len(value) == 2:
            return qs.filter(name__istartswith=value)
        else:
            return qs.filter(name__icontains=value)


class MatchFilter(FilterSet):
    homophily_strength = RangeFilter()
    heterophily_strength = RangeFilter()

    class Meta:
        model = Match
        fields = ['homophily_strength', 'heterophily_strength']
