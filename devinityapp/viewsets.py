from braces.views import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from rest_framework import status
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from rest_framework.decorators import detail_route, list_route
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from devinityapp.filters_api import FlagFilter, TagFilter, MatchFilter
from rest_framework.response import Response
from devinityapp.tracking_utils import mxp_report_skill, mxp_report_flagset
from stackexchange.seprovider import SEProvider
from django.core.paginator import Paginator
from devinityapp.serializers import *
from devinityapp.pagination import SimplePaginator
import viewsets_utils as view_utils
from rubicore import rubicore as rubi
from rubicore import tags_manager as rtm
from rubicore import rubicore_settings
from threading import Thread
from avatar_utils import *
from datetime import datetime


class UserViewSet(LoginRequiredMixin, ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @list_route(methods=['GET'])
    def me(self, request):
        serializer = self.serializer_class(
            User.objects.filter(id=request.user.id), many=True)
        return Response(data=serializer.data)

    @detail_route(
        methods=['post', 'put', 'patch', 'get'], permission_classes=[AllowAny],
        serializer_class=AvatarSerializer,
        parser_classes=(FormParser, MultiPartParser,)
    )
    def upload_avatar(self, request, pk=None, *args, **kwargs):
        if 'POST' in request.method or 'PATCH' in request.method:
            avatar = request.FILES.get('file')
            if not avatar:
                return Response(status=404)

            try:
                user = User.objects.get(id=pk)
            except User.DoesNotExist:
                return Response(status=404)
            else:
                request.FILES['file'] = request.FILES['file']
                filename = '%s.jpg' % user.id
                user.avatar.save(filename, avatar)
                process_uploaded_avatar(user.avatar, filename)
                user.avatar_thumbnail = create_thumbnail(user.avatar)
                user.save()
                headers = self.get_success_headers({'path': user.avatar.url})
                return Response({'path': user.avatar.url}, status=201,
                                headers=headers)
        else:
            avatar = User.objects.filter(id=pk).avatar
            page = self.paginate_queryset(avatar)
            serializer = self.get_paginated_response(page)
        return Response(serializer.data)


class SkillViewSet(ModelViewSet):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer

    def get_queryset(self):
        qs = super(SkillViewSet, self).get_queryset()
        user_id = int(self.request.query_params.get('user',
                                                    self.request.user.id))
        return qs.filter(user_id=user_id)

    def create(self, request, *args, **kwargs):
        mxp_report_skill(request)
        request.data.update({'user': request.user.id})
        return super(SkillViewSet, self).create(request, *args, **kwargs)

    @detail_route(methods=['GET'])
    def users(self, request, *args, **kwargs):
        """
        :returns: list of users from caller's org, having given skill,
        sorted by this skill level
        """
        skill = get_object_or_404(self.queryset, pk=self.kwargs['pk'])
        tag = skill.tag
        category = skill.familiar
        org = request.user.organization
        skills = Skill.objects\
            .filter(tag=tag)\
            .filter(familiar=category)\
            .filter(user__organization=org)\
            .exclude(user=request.user.id)\
            .order_by('-level')

        user_list = []
        for skill in skills:
            user_list.append(User.objects.get(id=skill.user.id))

        # list padding
        start = int(self.request.query_params.get('from', 0))
        end = int(self.request.query_params.get('to', len(user_list)))
        if end < start:
            end = start

        serializer = UserSerializer(user_list[start:end], many=True)
        return Response(serializer.data)

    @staticmethod
    def _get_tag_data(skill_data):
        tag = Tag.objects.get(id=skill_data['tag'])
        return TagSerializer(instance=tag).data

    def _get_data_with_tags(self, skills_data):
        data = skills_data
        for skill in data:
            skill['tag'] = self._get_tag_data(skill)
        return data

    @list_route(methods=['GET'])
    def favourites(self, request, *args, **kwargs):
        qs = self.get_queryset().filter(familiar=Skill.PASSIONATE)
        serializer = self.serializer_class(qs, many=True)
        return Response(data=self._get_data_with_tags(serializer.data))

    @list_route(methods=['GET'])
    def wanted(self, request, *args, **kwargs):
        qs = self.get_queryset().filter(familiar=Skill.WANT_TO_LEARN)
        serializer = self.serializer_class(qs, many=True)
        return Response(data=self._get_data_with_tags(serializer.data))

    @list_route(methods=['GET'])
    def owned(self, request, *args, **kwargs):
        qs = self.get_queryset().filter(familiar=Skill.KNOWS)
        serializer = self.serializer_class(qs, many=True)
        return Response(data=self._get_data_with_tags(serializer.data))

    @list_route(methods=['GET'])
    def rank_favourites(self, request, *args, **kwargs):
        return self.slice_top(self.ranking(request, Skill.PASSIONATE))

    @list_route(methods=['GET'])
    def rank_wanted(self, request, *args, **kwargs):
        return self.slice_top(self.ranking(request, Skill.WANT_TO_LEARN))

    @list_route(methods=['GET'])
    def rank_owned(self, request, *args, **kwargs):
        return self.slice_top(self.ranking(request, Skill.KNOWS))

    def ranking(self, request, category):
        qs = self.queryset\
            .filter(user__organization=request.user.organization)\
            .filter(familiar=category)\
            .values('tag')\
            .annotate(count=Count('tag'))\
            .order_by('-count')
        return qs

    def slice_top(self, queryset, num=3):
        """
        :returns: custom JSON of top :num: items from sorted list of top skills
        """
        queryset = queryset[:num]
        result = []
        for tag in queryset:
            element = {'tag': tag['tag'], 'count': tag['count']}
            result.append(element)
        return Response(data=self._get_data_with_tags(result))


class FlagViewSet(ModelViewSet):
    """
    ViewSet for Flag model
    """
    queryset = Flag.objects.all()
    serializer_class = FlagSerializer
    filter_class = FlagFilter

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data

        for flag_data in data:
            flag = get_object_or_404(Flag, id=flag_data['id'])
            flag_data['count'] = flag.get_count_for_organization(
                request.user.organization)

        return Response(data)

    def slice_top(self, queryset, request, num=3):
        """
        :returns: top :num: items from sorted list of top flags
        """
        qs = queryset.filter(user__organization=request.user.organization)\
            .annotate(num_users=Count('user'))\
            .order_by('-num_users')
        serializer = self.serializer_class(qs[:num], many=True)
        return Response(data=serializer.data)

    @detail_route(methods=['GET'])
    def users(self, request, pk=None):
        """
        :return: list of users in current organization flagged with given flag
        """
        flag = get_object_or_404(Flag, pk=pk)
        org = request.user.organization
        users = User.objects.filter(organization=org)\
            .exclude(id=request.user.id)\
            .filter(flagset__flag=flag)\
            .distinct()\
            .annotate(Count('id'))

        # list padding
        start = int(self.request.query_params.get('from', 0))
        end = int(self.request.query_params.get('to', len(users)))
        if end < start:
            end = start

        serializer = UserSerializer(users[start:end], many=True)
        return Response(serializer.data)

    @detail_route(methods=['GET'])
    def count(self, request, pk=None):
        """
        :return: number of users in current organization
        flagged with given flag
        """
        flag = get_object_or_404(Flag, pk=pk)
        cnt = FlagSet.objects\
            .filter(user__organization=request.user.organization)\
            .filter(flag=flag)\
            .distinct()\
            .count()
        return Response(cnt)


class FlagSetViewSet(ModelViewSet):
    """
    ViewSet for FlagSet model
    """
    queryset = FlagSet.objects.all()
    serializer_class = FlagSetSerializer

    def get_queryset(self):
        qs = super(FlagSetViewSet, self).get_queryset()
        user_id = int(self.request.query_params.get('user',
                                                    self.request.user.id))
        return qs.filter(user_id=user_id)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data

        for flag_set in data:
            flag_set['flag'] = FlagSerializer(
                instance=Flag.objects.get(id=flag_set['flag'])).data

        return Response(data=data)

    def create(self, request, *args, **kwargs):
        mxp_report_flagset(request)
        request.data.update({'user': request.user.id})
        return super(FlagSetViewSet, self).create(request, *args, **kwargs)

    @detail_route(methods=['GET'])
    def users(self, request, pk=None):
        """
        Returns list of users in current organization flagged with given flag
        """
        flag = self.get_object().flag
        org = request.user.organization
        users = User.objects.filter(organization=org)\
            .filter(flagset__flag=flag)\
            .exclude(id=request.user.id)\
            .distinct()\
            .annotate(Count('id'))

        # list padding
        start = self.request.query_params.get('from', 0)
        end = self.request.query_params.get('to', len(users))
        if end < start:
            end = start

        serializer = UserSerializer(users[start:end], many=True)
        return Response(serializer.data)


class OrganizationViewSet(ModelViewSet):
    """
    ViewSet for Organization model
    """
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


class TagViewSet(ModelViewSet):
    """
    ViewSet for Tag model
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_class = TagFilter
    # page_size = 10
    # max_page_size = 10

    # TODO: this override makes no sense - nothing is changed
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset[:20], many=True)
        return Response(serializer.data)

    @detail_route(methods=['GET'])
    def wiki(self, request, pk=None):
        tag = self.get_object()

        if hasattr(tag, 'wiki'):
            return self.return_wiki_data(tag.wiki)
        else:
            try:
                se_response = SEProvider().get_tag_wiki(tag.name)
                description = se_response[0].excerpt
                new_wiki = TagWiki.objects.create(description=description)
                tag.wiki = new_wiki
                tag.save()
            except IndexError:
                new_wiki = TagWiki.objects.create(
                    description='no description available')

            return self.return_wiki_data(new_wiki)

    def return_wiki_data(self, wiki):
        serializer = TagWikiSerializer(instance=wiki)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def rank(self, request, *args, **kwargs):
        pass

    @detail_route(methods=['GET'])
    def users(self, request, *args, **kwargs):
        tag = self.get_object()
        query = {
            'organization': request.user.organization,
            'skill__tag': tag
        }

        if 'to_learn' in request.query_params:
            query['skill__familiar'] = Skill.WANT_TO_LEARN
        elif 'owned' in request.query_params:
            query['skill__familiar'] = Skill.KNOWS

        users = User.objects.filter(**query)
        paginator = SimplePaginator()

        page = paginator.paginate_queryset(users, request)

        if page is not None:
            serializer = UserSerializer(page, many=True)
            return Response(data=serializer.data)
        else:
            serializer = UserSerializer(users, many=True)
            return Response(data=serializer.data)

    @detail_route(methods=['GET'])
    def count_known(self, request, pk=None):
        return self.count_by_category(request, pk, Skill.KNOWS)

    @detail_route(methods=['GET'])
    def count_passion(self, request, pk=None):
        return self.count_by_category(request, pk, Skill.PASSIONATE)

    @detail_route(methods=['GET'])
    def count_aim(self, request, pk=None):
        return self.count_by_category(request, pk, Skill.WANT_TO_LEARN)

    def count_by_category(self, request, pk=None, category=Skill.KNOWS):
        """
        :return: number of mappings within organization by category
        """
        tag = self.get_object()
        org = request.user.organization
        cnt = Skill.objects.filter(tag=tag) \
            .filter(familiar=category) \
            .filter(user__organization=org) \
            .distinct() \
            .count()
        return Response(cnt)


class TagWikiViewSet(ModelViewSet):
    """
    APIView for TagWiki model
    """
    queryset = TagWiki.objects.all()
    serializer_class = TagWikiSerializer

    def retrieve(self, request, pk=None, **kwargs):
        # TODO: these try/except blocks are broken
        try:
            description = self.queryset.get(id=pk).description
        except ObjectDoesNotExist:
            try:
                tag = Tag.objects.get(id=pk)
                tag_name = tag.name
                se_response = SEProvider().get_tag_wiki(tag_name)
                description = se_response[0].excerpt
                tw = TagWiki(id=pk, description=description)
                tw.save()
                tag.wiki = tw
                tag.save()
            except ObjectDoesNotExist:
                description = 'invalid tag'
        finally:
            description = 'invalid entry'
        # TODO: BTW: we will never make API call for non-existent TagWiki
        return super(TagWikiViewSet, self).retrieve(self, request, pk)


class MatchViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    filter_class = MatchFilter

    def get_queryset(self):
        qs = super(MatchViewSet, self).get_queryset()
        username = self.request.query_params.get('username', None)
        userid = self.request.query_params.get('userid', None)

        if username is not None:
            qs = qs.filter(user_1__login=username)\
                .order_by("-homophily_strength")
        elif userid is not None:
            qs = qs.filter(user_1=userid).order_by("-homophily_strength")
        elif u'pk' in self.kwargs:
            qs = qs.filter(user_1=self.kwargs[u'pk'])\
                .order_by("-homophily_strength")
        else:
            qs = qs.filter(user_1=self.request.user)\
                .order_by("-homophily_strength")
        return qs

    def retrieve(self, request, pk=None, **kwargs):
        queryset = Match.objects.all()
        match = get_object_or_404(queryset, pk=pk)
        serializer = MatchSerializer(match)
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    def report_match(self, request, pk=None, **kwargs):
        match = get_object_or_404(self.queryset, id=pk)
        user = self.request.user
        try:
            existing_feedback = MatchRejection.objects.get(match=match,
                                                           submitter=user)
            existing_feedback.timestamp = datetime.now()
        except MatchRejection.DoesNotExist:
            MatchRejection.objects.create(match=match, submitter=user)
        finally:
            return Response({'status': 'match rejection reported'})


    @list_route(methods=['GET'])
    def matches_homo(self, request, *args, **kwargs):
        # you can utilize params 'userid', 'from', 'to', 'legacy_match_reasons'
        start = view_utils.unicodetoint(self.request.query_params.get('from', None))
        end = view_utils.unicodetoint(self.request.query_params.get('to', None))
        current_page = view_utils.unicodetoint(self.request.query_params.get('page', None))
        page_size = view_utils.unicodetoint(self.request.query_params.get('page_size', 10))
        legacy_match_reasons = view_utils.unicodetoint(self.request.query_params.get('legacy_match_reasons', 0))

        if (current_page is not None) and (any([start, end])):
            return Response({'detail' : "Invalid mix of arguments. Use either pagination or from/to parameters", 'args' : 
                [x for x in ['page', 'from', 'to'] if x is not None]}, 
                     status = status.HTTP_400_BAD_REQUEST)

        if (start is not None) and (start < 0):
            return Response({'detail' : "Parameter must be a positive number", 'args' : ['from']}, 
                     status = status.HTTP_400_BAD_REQUEST)

        if (end is not None):
            if (end < 1):
                return Response({'detail' : "Parameter must be greater than zero", 'args' : ['to']}, 
                         status = status.HTTP_400_BAD_REQUEST)
            if (start is not None) and (end-start < 1):
                return Response({'detail' : "Invalid range of slice arguments", 'args' : ['from', 'to']}, 
                         status = status.HTTP_400_BAD_REQUEST)

        if (current_page is not None):
            if (current_page < 1):
                return Response({'detail' : "Invalid page number", 'args' : ['current_page']}, 
                         status = status.HTTP_400_BAD_REQUEST)
            if (page_size < 1):
                return Response({'detail' : "Invalid page size", 'args' : ['page_size']}, 
                         status = status.HTTP_400_BAD_REQUEST)

        if not any([start, end, current_page]):
            # we list first 10 matches (or as much as given in param page_size)
            current_page = 1

        userid = self.request.query_params.get('userid', self.request.user)

        # Load the actual data here
        matches = self.queryset.filter(user_1=userid, homophily_strength__gt=0)

        if current_page is None:
            matches = matches.order_by('-homophily_strength', 'user_2_id')[start:end]  # safe, if start is too high, [] is returned
        else:
            matches = Paginator(matches.order_by('-homophily_strength', 'user_2_id')[1:], page_size)
            matches = matches.page(current_page).object_list

        for match in matches:
            match.reason.description = view_utils.translate_similar_result(match.reason.shared_skills, legacy_match_reasons)
        
        if not matches:
            return Response(data=[])
        if len(matches) > 1:
            serializer = MatchSerializer(matches, many=True)
        else:
            serializer = MatchSerializer(matches)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def matches_hetero(self, request, *args, **kwargs):
        # you can utilize params 'userid', 'from', 'to', 'newbies', 'legacy_match_reasons'
        start = view_utils.unicodetoint(self.request.query_params.get('from', None))
        end = view_utils.unicodetoint(self.request.query_params.get('to', None))
        current_page = view_utils.unicodetoint(self.request.query_params.get('page', None))
        page_size = view_utils.unicodetoint(self.request.query_params.get('page_size', 10))
        legacy_match_reasons = view_utils.unicodetoint(self.request.query_params.get('legacy_match_reasons', 0))

        if (current_page is not None) and (any([start, end])):
            return Response({'detail' : "Invalid mix of arguments. Use either pagination or from/to parameters", 'args' : 
                [x for x in ['page', 'from', 'to'] if x is not None]}, 
                     status = status.HTTP_400_BAD_REQUEST)

        if (start is not None) and (start < 0):
            return Response({'detail' : "Parameter must be a positive number", 'args' : ['from']}, 
                     status = status.HTTP_400_BAD_REQUEST)

        if (end is not None):
            if (end < 1):
                return Response({'detail' : "Parameter must be greater than zero", 'args' : ['to']}, 
                         status = status.HTTP_400_BAD_REQUEST)
            if (start is not None) and (end-start < 1):
                return Response({'detail' : "Invalid range of slice arguments", 'args' : ['from', 'to']}, 
                         status = status.HTTP_400_BAD_REQUEST)

        if (current_page is not None):
            if (current_page < 1):
                return Response({'detail' : "Invalid page number", 'args' : ['current_page']}, 
                         status = status.HTTP_400_BAD_REQUEST)
            if (page_size < 1):
                return Response({'detail' : "Invalid page size", 'args' : ['page_size']}, 
                         status = status.HTTP_400_BAD_REQUEST)

        if not any([start, end, current_page]):
            # we list first 10 matches (or as much as given in param page_size)
            current_page = 1

        try:
            student = kwargs['student']
        except KeyError:
            student = True
        userid = self.request.query_params.get('userid', self.request.user)

        if student:
            matches = self.queryset.filter(user_2=userid, heterophily_strength__gt=0)
        else:
            matches = self.queryset.filter(user_1=userid, heterophily_strength__gt=0)

        if current_page is None:
            matches = matches.order_by('-heterophily_strength', '-homophily_strength', 'user_1_id' if student else 'user_2_id')[start:end]  # safe, if start is too high, [] is returned
        else:
            matches = Paginator(matches.order_by('-heterophily_strength', '-homophily_strength', 'user_1_id' if student else 'user_2_id')[1:], page_size)
            matches = matches.page(current_page).object_list

        for match in matches:
            match.match_type = 'student' if student else 'teacher'
            if student:
                match.user_2, match.user_1 = match.user_1, match.user_2
                match.reason.description = view_utils.translate_student_result(match.reason.right_teaches_left, legacy_match_reasons)
            else:
                match.reason.description = view_utils.translate_teacher_result(match.reason.left_teaches_right, legacy_match_reasons)

        if not matches:
            return Response(data=[])
        if len(matches) > 1:
            serializer = MatchSerializer(matches, many=True)
        else:
            serializer = MatchSerializer(matches)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def matches_all(self, request, batch_size=1, *args, **kwargs):
        # you can utilize params 'userid', 'page', 'legacy_match_reasons'
        current_page = self.request.query_params.get('page', 1)
        legacy_match_reasons = view_utils.unicodetoint(self.request.query_params.get('legacy_match_reasons', 0))
        userid = self.request.query_params.get('userid', self.request.user)
        if batch_size is not 1:
            raise NotImplementedError

        result_objects = []

        homophily_matches = self.queryset.filter(user_1=userid,
                                                 homophily_strength__gt=0)
        homophily_matches = Paginator(homophily_matches.order_by('-homophily_strength', 'user_2_id'), batch_size)
        homophily_match = homophily_matches.page(current_page).object_list.first()
        if homophily_match is not None:
            homophily_match.match_type = 'similar'
            homophily_match.reason.description = view_utils.translate_similar_result(homophily_match.reason.shared_skills, legacy_match_reasons)
            result_objects.append(homophily_match)

        heterophily_matches = self.queryset.filter(user_2=userid,
                                                   heterophily_strength__gt=0)
        heterophily_matches = Paginator(heterophily_matches.order_by('-heterophily_strength', '-homophily_strength', 'user_1_id'), batch_size)
        heterophily_match = heterophily_matches.page(current_page).object_list.first()
        if heterophily_match is not None:
            heterophily_match.match_type = 'student'
            heterophily_match.user_2, heterophily_match.user_1 = heterophily_match.user_1, heterophily_match.user_2
            heterophily_match.reason.description = view_utils.translate_student_result(heterophily_match.reason.right_teaches_left, legacy_match_reasons)
            result_objects.append(heterophily_match)

        heterophily_rev_matches = self.queryset.filter(user_1=userid,
                                                       heterophily_strength__gt=0)
        heterophily_rev_matches = Paginator(heterophily_rev_matches.order_by('-heterophily_strength', '-homophily_strength', 'user_2_id'), batch_size)
        heterophily_rev_match = heterophily_rev_matches.page(current_page).object_list.first()
        if heterophily_rev_match is not None:
            heterophily_rev_match.match_type = 'teacher'
            heterophily_rev_match.reason.description = view_utils.translate_teacher_result(heterophily_rev_match.reason.left_teaches_right, legacy_match_reasons)
            result_objects.append(heterophily_rev_match)

        if not result_objects:
            return Response(data=[])
        serializer = MatchSerializer(result_objects, many=True)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def similar(self, request, *args, **kwargs):
        return self.matches_homo(request, *args, **kwargs)

    @list_route(methods=['GET'])
    def students(self, request, *args, **kwargs):
        kwargs["student"] = True
        return self.matches_hetero(request, *args, **kwargs)

    @list_route(methods=['GET'])
    def teachers(self, request, *args, **kwargs):
        kwargs["student"] = False
        return self.matches_hetero(request, *args, **kwargs)

class RubicoreViewSet(GenericViewSet):

    @list_route(methods=['GET'])
    def execute_all(self, request, *args, **kwargs):
        no_celery = view_utils.unicodetoint(self.request.query_params.get('no_celery', 0))
        if no_celery:
            thread = Thread(target = rubi.integrity_watcher, args = ([1],))
            thread.start()
        else:
            rubi.integrity_watcher.delay([1])
        content = {'status': 'yay, all launched' + ('!' if no_celery else ' (with celery)!')}
        return Response(content)

    @list_route(methods=['GET'])
    def execute(self, request, *args, **kwargs):
        no_celery = view_utils.unicodetoint(self.request.query_params.get('no_celery', 0))
        if no_celery:
            thread = Thread(target = rubi.full_launcher, args = (request.user.organization,))
            thread.start()
        else:
            rubi.full_launcher.delay(request.user.organization.id)
        content = {'status': 'yay, launched' + ('!' if no_celery else ' (with celery)!')}
        return Response(content)

    @list_route(methods=['GET'])
    def annotate(self, request, *args, **kwargs):
        no_celery = view_utils.unicodetoint(self.request.query_params.get('no_celery', 0))
        if no_celery:
            thread = Thread(target = rtm.annotate, args = (None,))
            thread.start()
        else:
            rtm.annotate.delay(None)
        content = {
            'status': 'yay, launched' +
                 ('!' if no_celery else ' (with celery)!')
         }
        return Response(content)

    @list_route(methods=['GET'])
    def switch_version(self, request, *args, **kwargs):
        new_version = view_utils.unicodetoint(
            self.request.query_params.get('new_version', 2))
        re_run = view_utils.unicodetoint(
            self.request.query_params.get('re_run', 0))
        no_celery = view_utils.unicodetoint(
            self.request.query_params.get('no_celery', 0))
        if not rubicore_settings.is_legit_version(new_version):
            content = {'status': 'failure (wrong version number)',
                       'immediate_recalculate': 'false'}
        else:
            rubicore_settings.set_version(new_version)
            if re_run:
                if no_celery:
                    thread = Thread(target=rubi.full_launcher,
                                    args=(request.user.organization,))
                    thread.start()
                else:
                    rubi.full_launcher.delay(request.user.organization.id)
            content = {
                'status': 'mechanism changed to ' + str(new_version),
                'immediate_recalculate': 'true' if re_run else 'false'
            }
        return Response(content)
