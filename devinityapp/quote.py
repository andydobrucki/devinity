# coding: utf-8

'''
Purpose of this code is to produce random profile quote /aka motto/ for users lazy enough to leave field empty
'''

import random

predefined_mottos = [
    "One test is worth a thousand expert opinions",
    "Life is too short to build something no one wants",
    "There is only one way to avoid criticism - do nothing, say nothing and be nothing",
    "Any darn fool can make something complex but it takes a genius to make something simple",
    "Example isn't another way to teach. It's the only way to teach",
    "The value is in what gets used, not in what gets built",
    "Boring and effective are mutually exclusive attributes in learning",
    "Attitudes are contagious. Is yours worth catching?",
    "The secret of getting ahead is getting started",
    "What good is an idea if it remains an idea? Try, experiment, iterate, fail. Try again. Change the world.",
    "Be great. Focus on collaboration instead of competition",
]


def random_quote():
    return random.choice(predefined_mottos)
