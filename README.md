Setup Django on EC2 Server:

1. Download code and cp it to the EC2 server using scp command:
scp -i <key.pem> -r devinity(name of folder) ubuntu@SERVER-IP: DIR_PATH where you want to place the project code.

2. Run apt update and upgrade command:
apt update && apt upgrade

3. Install Python 2, Nginx, DB:
apt install python2.7 nginx mysql-server libmysqlclient-dev

4. Uninstall anyother version of python you have on system:
apt remove python3 --purge

5. Install pip:
apt install python-pip

6. Create DB:
            mysql -uroot –p
            MariaDB [(none)]> CREATE DATABASE dong;
            MariaDB [(none)]> GRANT ALL PRIVILEGES ON dong.* TO 'dong'@'localhost'  IDENTIFIED BY 'Default1234';
 MariaDB [(none)]> FLUSH PRIVILEGES;
      MariaDB [(none)]> \q

 These DB settings should be set in devinty/settings/local.py file.

7. Install dgengo app dependencies:
pip install -r requirements.txt

All the dependencies will be installed to run the app. 
8. now its time to run migrate:
python manage.py migrate 


9. Run the server:
python manage.py runserver 0.0.0.0:8000 

Step for installing dependencies for Devinity

First install python2 and pip2 in Ubuntu (Ubuntu already have python2 by default so you have to install only pip2)
Command for that is `sudo apt-get install python-pip`
Then you have to create virtual environment of python2  so that default python will not make conflicts of any dependency
Step 1: Install Virtualenv
 	First, we will update our apt-get, then we will install the module virtualenv module.
```
sudo apt-get update
sudo apt-get install python-virtualenv
```

Step2: Create a virtualenv
If your default python is python2 in most cases it is default then run the following command in that directory where you want to make virtual env (like project directory )
virtualenv env_name 
if your default python is python 3 then run the command like this virtualenv –p path of python2 env_name
Step3: Activate the virtual environment that you have made
source env_name/bin/activate
you would see the env name in your terminal
After that you have to install all the dependency of Django project for this you have to move to your project directory and run the following command 
pip install –r requirements.txt
if you have an error in MySql package you would to install sql client first for this run the following command
sudo apt-get install libmysqlclient-dev
after that run pip install –r requirements.txt again
your python dependencies has been install and after setting your database you can run the migration first for this run the following command
python manage.py migrate
Now run the Django server with python manage.py runserver
