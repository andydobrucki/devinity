/**
 * Created by wojciech on 30.10.15.
 */
var webpack = require('webpack');
var path = require('path');

module.exports = {
  watch: true,
  entry: './src/app.js',
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel-loader', query: {stage: 0}},
      {test: /\.html$/, loader: 'html'}
    ]
  },
  devtool: '#source-map',
  output: {
    path: __dirname,
    filename: 'devinity.min.js'
  },
    plugins: [
      //new webpack.optimize.UglifyJsPlugin({
      //  mangle: true
      //})
    ]
};
