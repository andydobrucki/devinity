import {
  AccountManager,
  FlagSectionDirective,
  FlagsSelectDirective,
  InfinityScroll,
  LoveStuffSectionDirective,
  Match,
  MatchingSectionDirective,
  OnEnter,
  ProfileSectionDirective,
  SearchSection,
  SetFocus,
  SkillRankingListDirective,
  SkillSectionDirective,
  WantedStuffDirective,
  WantStuffRankingDirective
} from './components';

import {Devinity} from './services';

import FlagRankingController from './controllers/flag_ranking';
import FlagSectionController from './controllers/flag_section';
import LoveStuffSectionController from './controllers/love_stuff_section';
import MatchingSectionController from './controllers/matching_section';
import ModalInfoController from './controllers/modal_info';
import PeerMatchController from './controllers/peer_match'
import ProfileSectionController from './controllers/profile_section';
import RankingTagModalController from './controllers/ranking_tag_modal';
import SearchSectionController from './controllers/search_section';
import SkillRankingListController from './controllers/skill_ranking';
import SkillSectionController from './controllers/skill_section';
import WantedStuffController from './controllers/wanted_section';
import WantStuffRankingController from './controllers/wanted_ranking';

export default
angular.module('devinity', [
  'ui.bootstrap',
  'restangular',
  'toastr',
  'ngAnimate',
  'ngSanitize',
  'ngCookies',
  'ngTagsInput',
  'xeditable',
  'ui.select',
  'wr-image',
  'angular-send-feedback',
  'infinite-scroll',
  'flow'
])
  .service('Devinity', Devinity)

  .controller('FlagRankingCtrl', FlagRankingController)
  .controller('FlagSectionCtrl', FlagSectionController)
  .controller('LoveStuffSectionCtrl', LoveStuffSectionController)
  .controller('MatchingSectionCtrl', MatchingSectionController)
  .controller('ModalInfoCtrl', ModalInfoController)
  .controller('PeerMatchCtrl', PeerMatchController)
  .controller('ProfileSectionCtrl', ProfileSectionController)
  .controller('RankingTagModalCtrl', RankingTagModalController)
  .controller('SearchSectionCtrl', SearchSectionController)
  .controller('SkillRankingCtrl', SkillRankingListController)
  .controller('SkillSectionCtrl', SkillSectionController)
  .controller('WantedRankingCtrl', WantStuffRankingController)
  .controller('WantedStuffCtrl', WantedStuffController)

  .directive('accountManager', () => new AccountManager())
  .directive('devFlagSection', () => new FlagSectionDirective())
  .directive('devFlagsSelect', () => new FlagsSelectDirective())
  .directive('devLoveStuffSection', () => new LoveStuffSectionDirective())
  .directive('devMatch', () => new Match())
  .directive('devPeerMatchingSection', () => new MatchingSectionDirective())
  .directive('devProfileSection', () => new ProfileSectionDirective())
  .directive('devSearchSection', () => new SearchSection())
  .directive('devSkillRanking', () => new SkillRankingListDirective())
  .directive('devSkillSection', () => new SkillSectionDirective())
  .directive('devWantedStuffSection', () => new WantedStuffDirective())
  .directive('devWantStuffRanking', () => new WantStuffRankingDirective())
  .directive('infinityScroll', () => new InfinityScroll())
  .directive('onEnter', () => new OnEnter())
  .directive('setFocus', SetFocus.factory)

  .config(['RestangularProvider', (RestangularProvider) => {
    RestangularProvider.setBaseUrl('/api/');
    RestangularProvider.setRequestSuffix('/');
  }])
  .config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
      target: '/',
      permanentErrors: [404, 500, 501],
      maxChunkRetries: 1,
      chunkRetryInterval: 5000,
      simultaneousUploads: 4,
      singleFile: true,
      testChunks: false
    }
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
  }])
  .run(['Restangular', '$cookies', 'flowFactory', (Restangular, $cookies, flowFactory) => {
    Restangular.setDefaultHeaders({
      'X-CSRFTOKEN': $cookies.get('csrftoken')
    });
  }])
  .run(['$templateCache', $templateCache => {
    $templateCache.put("bootstrap/match-multiple.tpl.html", require('./templates/custom_select_tag.html'))
    $templateCache.put("bootstrap/select-multiple.tpl.html","<div class=\"ui-select-container ui-select-multiple ui-select-bootstrap dropdown form-control\" ng-class=\"{open: $select.open}\"><div><div class=\"ui-select-match\"></div><input type=\"text\" autocomplete=\"false\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" class=\"ui-select-search input-xs\" placeholder=\"add more\" ng-disabled=\"$select.disabled\" ng-hide=\"$select.disabled\" ng-click=\"$select.activate()\" ng-model=\"$select.search\" role=\"combobox\" aria-label=\"{{ $select.baseTitle }}\" ondrop=\"return false;\"></div><div class=\"ui-select-choices\"></div></div>");
    $templateCache.put('accountManager.html', require('./templates/accountManager.html'))
    $templateCache.put('flagAutoComplete.html', require('./templates/flagAutocomplete.html'));
    $templateCache.put('flagSelection.html', require('./templates/flagSelection.html'));
    $templateCache.put('flagsSelect.html', require('./templates/flagsSelect.html'));
    $templateCache.put('modalInfo.html', require('./templates/modalInfo.html'));
    $templateCache.put('peerMatch.html', require('./templates/peerMatch.html'))
    $templateCache.put('peerMatchingSection.html', require('./templates/peerMatchingSection.html'))
    $templateCache.put('skillRanking.html', require("./templates/skillRanking.html"));
    $templateCache.put('wantsRanking.html', require('./templates/wantsRanking.html'));
    
  }])

  .run(['editableOptions', 'editableThemes', (editableOptions, editableThemes)  => {
    editableOptions.theme = 'bs3';
    editableThemes.bs3.buttonsClass = 'btn-xs';
  }])
