import {DEFAULT_AVATAR} from './../constants'

class RankingTagModalController {
	constructor(Restangular, tagId, description, toLearn) {
		this.Restangular = Restangular;
		this.page = 1;
		this.users = [];
		this.tagId = tagId;
		this.toLearn = toLearn;
		this.description = description;

		this.userListTitle = toLearn ? 'Want to learn this skill' : 'Users with this skill';

		this.getTagUsers();
	}

	loadMoreUsers() {
		if (!this.allUsersLoaded) {
			this.getTagUsers()
		}
	}

	goTo(url) {
		window.location.href = url;
	}

	getTagUsers() {
		this.loadingUsers = true;

		let users = this.Restangular.one('tag', this.tagId).all('users');
			if (this.toLearn) {
				users = users.getList({page: this.page, to_learn: ''})
			} else {
				users = users.getList({page: this.page, owned: ''})
			}

		users.then(this.assignUsers.bind(this))
	}

	scrollEnded() {
		this.loadMoreUsers();
	}

	getHeight() {
    return 600;
  }

	assignUsers(users) {
		users = users.length ? users.plain() : [];

		this.users = this.users.concat(users);
		this.users = this.users.map(this.checkAvatar)
		this.page += 1;
		this.loadingUsers = false;
		this.allUsersLoaded = Boolean(users.length < 10 || !users.length);
	}

	checkAvatar(user) {
    user.avatar = user.avatar || DEFAULT_AVATAR
    return user
  }

	static $inject = ['Restangular', 'tagId', 'description', 'toLearn']
}

export default RankingTagModalController
