/**
 * Created by Wojciech Rydel <rydel.wojciech@gmail.com> on 21.11.15.
 */

class LoveStuffSectionController {
  constructor(Devinity, $uibModal, Restangular, toastr, $q) {
    this.Restangular = Restangular;
    this._search = '';
    this.Devinity = Devinity;
    this.modal = $uibModal;
    this.toastr = toastr;
    this.skills = [];
    this.tags = [];
    this.q = $q;
    this.initializing = true;
    var userId = parseInt(window.localStorage.getItem('userId'));
    var query = {};
    if(userId) {
      query.user = userId
    }
    this.Restangular.all('skills/favourites').getList(query)
      .then(result => {
        this.skills = result.plain();
        this.tags = _.map(this.skills, skill => skill.tag);
        this.initializing = false;
      })
  }

  editLoveStuffs() {
    this.editing = true;
    this.userFlagsCopy = angular.copy(this.skills)
  };

  saveLoveStuffs() {
    this.saving = true;
    var previousTagsIds = _.map(this.userFlagsCopy, skill => skill.tag.id);
    var currentTagsIds = _.map(this.tags, tag => tag.id);
    var tagsToDelete = _.reject(previousTagsIds, id => _.includes(currentTagsIds, id));
    var tagsToAdd = _.reject(currentTagsIds, id => _.includes(previousTagsIds, id));
    var skillsToDelete = _.filter(this.userFlagsCopy, skill => _.includes(tagsToDelete, skill.tag.id));
    var deletePromises = _.map(skillsToDelete, skill => this.Restangular.one('skills',  skill.id).remove());

    this.q.all(deletePromises).then(result => {
      this.skills = _.reject(this.skills, skill => _.includes(_.map(skillsToDelete, skill => skill.id), skill.id));
      this.q.all(_.map(tagsToAdd, tagId => this.Restangular.all('skills').post({
        tag: tagId,
        familiar: this.Devinity.familiar.passionate
      })))
        .then(addedSkills => {
          _.forEach(addedSkills, skill => {
            skill.tag = _.filter(this.tags, tag => tag.id == skill.tag)[0]
          });
          this.skills = _.union(this.skills, addedSkills);
          this.editing = false;
          this.saving = false;
          this.toastr.success('', 'Updated stuff you love')
        })
    });
  };

  cancelLoveStuffsEditing() {
    this.editing = false;
    this.skills = this.userFlagsCopy;
    this.tags = _.map(this.skills, skill => skill.tag)
  }

  getChoices(search) {
    if(search && search.length > 0) {
      this.searching = true;
      this.Restangular.all('tag').getList({
        name: search,
        exclude:_.map(this.tags, tag => tag.id)
      })
        .then(result => {
          this.choices = result.plain();
          this.searching = false
        })
    }
  }

  showInfo(skill) {
    this.Devinity.showPassionateSkillInfo(skill);
  }

  static $inject = ['Devinity', '$uibModal', 'Restangular', 'toastr', '$q']
}

export default LoveStuffSectionController
