import {DEFAULT_AVATAR} from './../constants'

class ProfileSectionController {
  user;
  editingQuote;
  editingProfile;
  _quoteCopy;
  _userCopy;
  Restangular;
  toastr;
  scope;

  constructor(Restangular, $scope, toastr, $rootScope, flowFactory, $cookies) {
    this.Restangular = Restangular;
    this.toastr = toastr;
    this.scope = $scope;
    this.editingProfile = false;
    this.editingQuote = false;
    this.rootScope = $rootScope;
    this.$cookies = $cookies;
    this.flowFactory = flowFactory;

    this.getUserData();
  }

  getUserData() {
    var userId = parseInt(window.localStorage.getItem('userId'));
    this.initializing = true;

    if(userId) {
      this.editable = false;
      this.getOtherUserProfile(userId)
    } else {
      this.editable = true;
      this.getLoggedUserProfile()
    }
  };

  getHeaders(){
    return {'X-CSRFTOKEN': this.$cookies.get('csrftoken')}
  }

  showFile($file, $event, $flow) {
    console.log('file', $file);
    console.log('event', $event);
    console.log('flow', $flow);
    debugger
  }

  getLoggedUserProfile() {
    this.Restangular.all('users/me').getList()
      .then(userProfile => {
        this.user = userProfile[0];
        this.user.iconClass = 'devicon-angularjs-plain';
        this.user.photo = this.user.avatar || DEFAULT_AVATAR;
        this.initializing = false;
      })
  }

  getOtherUserProfile(userId) {
    this.Restangular.one('users', userId).get()
      .then(userProfile => {
        this.user = userProfile.plain()
        this.user.iconClass = 'devicon-angularjs-plain'
        this.user.photo = this.user.avatar || DEFAULT_AVATAR
        this.initializing = false
      })
  }

  editProfile() {
    var vm = this;
    this._userCopy = angular.copy(this.user);
    this.editingProfile = true;

  }

  saveProfile() {
    this.editingProfile = false;
    this.rootScope.$broadcast('devinity:user', this.user);
    this.Restangular.one('users', this.user.id)
      .patch({
        name: this.user.name,
        position: this.user.position,
        email: this.user.email
      })
      .then(user => {
        this.toastr.success('', 'Profile updated!')
      }, error => {
        this.toastr.error('', 'Error occurred!')
      })
  }

  fileAdded($file, $message, $flow) {
    let response = JSON.parse($message)

    this.user.photo = response.path
    this.photoUploading = false
    this.toastr.success('', 'Avatar updated')
    this.rootScope.$broadcast('avatar:updated', this.user.photo)
  }

  fileUploading() {
    this.photoUploading = true
  }

  exitPhotoEdit() {
    this.editPhoto = false
  }

  changePhoto() {
    this.editPhoto = true;
  }

  cancelProfileEdition() {
    this.user = this._userCopy;
    this.editingProfile = false;
  }

  editQuote() {
    var vm = this;
    vm.editingQuote = true;
    vm._quoteCopy = angular.copy(vm.user.motto);
  };

  saveQuote() {
    this.quote = this._quoteCopy;
    this.editingQuote = false;
    this.Restangular.one('users', this.user.id)
      .patch({motto: this.user.motto})
      .then(success => {
        this.toastr.success('', 'Quote updated')
      }, error => {
        this.toastr.error('', 'Error occurred!')
      })
  }

  cancelQuoteEdition() {
    this.editingQuote = false
  }

  static $inject = ['Restangular', '$scope', 'toastr', '$rootScope', 'flowFactory', '$cookies']
}

export default ProfileSectionController
