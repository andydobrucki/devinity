import {DEFAULT_AVATAR} from './../constants'

class ModalInfoController {
  $modalInstance;
  description;

  constructor($modalInstance, Devinity, description, users, modalType, skillId, Restangular) {
    this.allUsersLoaded = false;
    this.usersFrom = 0;
    this.usersTo = 10;
    this.skill = Restangular.one('skills', skillId);
    this.flag = Restangular.one('flag', skillId);
    this.tag = Restangular.one('tag', skillId);
    this.isSkillModal = Boolean(modalType !== Devinity.modalTypes.flag && modalType !== Devinity.modalTypes.ranking);
    this.$modalInstance = $modalInstance;
    this.description = description;
    this.modalType = modalType;
    this.users = [];
    this.modalTitle = (modalType == Devinity.modalTypes.flag) ? 'Flag View' : 'Tag View';
    this.userListTitle = (modalType == Devinity.modalTypes.flag) ?
      'Also raised this flag' : (modalType == Devinity.modalTypes.owned) ?
      `Also have this tag ${description.title}` : (modalType == Devinity.modalTypes.passionate) ?
      `Also passionate about ${description.title}` : `Also want to learn ${description.title}`;
    this.Devinity = Devinity;

    if (this.isSkillModal) {
      this.loadSkillUsers();
    } else if (modalType == this.Devinity.modalTypes.ranking) {
      this.loadTagUsers();
    } else {
      this.loadFlagUsers();
    }
  };

  goTo(url) {
    window.location.href = url;
  }

  scrollEnded() {
    if (this.isSkillModal) {
      this.loadSkillUsers();
    } else if (this.modalType == this.Devinity.modalTypes.ranking) {
      this.loadTagUsers();
    } else {
      this.loadFlagUsers();
    }
  }

  getHeight() {
    return 600
  }

  loadFlagUsers() {
    if (!this.allUsersLoaded) {
      this.loadingUsers = true;

      this.flag.all('users').getList({from: this.usersFrom, to: this.usersTo})
        .then(result => {
          this.usersFrom = this.usersTo + 1;
          this.usersTo = this.usersFrom + 10;

          if(result.length) {
            this.users = this.users.concat(result.plain());
            this.users = this.users.map(this.checkAvatar)
            this.allUsersLoaded = (result.length < 10)
          } else {
            this.allUsersLoaded = true;
          }
          this.loadingUsers = false;
        })
    }
  }

  loadSkillUsers() {
    if(!this.allUsersLoaded) {
      this.loadingUsers = true;
      this.skill.all('users').getList({from: this.usersFrom, to: this.usersTo})
      .then(users => {
        this.usersFrom = this.usersTo + 1;
        this.usersTo = this.usersFrom + 10;
        if(users.length) {
          this.users = this.users.concat(users.plain());
          this.users = this.users.map(this.checkAvatar)
          this.allUsersLoaded = (users.length < 10)
        } else {
          this.allUsersLoaded = true;
        }
        this.loadingUsers = false;
      })
    }
  }

  loadTagUsers() {
    if (!this.allUsersLoaded) {
      this.loadingUsers = true;
      this.usersFrom += 1;
      this.tag.all('users').getList({page: this.usersFrom})
        .then(users => {
          users = users.length ? users.plain() : []

          if (users.length) {
            this.users = this.users.concat(users);
            this.users = this.users.map(this.checkAvatar)
            this.allUsersLoaded = (users.length < 10)
          } else {
            this.allUsersLoaded = true
          }

          this.loadingUsers = false;
        })
    }
  }

  checkAvatar(user) {
    user.avatar = user.avatar || DEFAULT_AVATAR
    return user
  }

  closeModal() {
    this.$modalInstance.dismiss()
  }

  static $inject = ['$uibModalInstance', 'Devinity', 'description', 'users', 'modalType', 'skillId', 'Restangular']
}

export default ModalInfoController
