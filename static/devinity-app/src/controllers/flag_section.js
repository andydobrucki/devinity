/**
 * Created by Wojciech Rydel <rydel.wojciech@gmail.com> on 21.11.15.
 */

class FlagSectionController {
  choices = [];
  edit = false;

  constructor(Restangular, $q, toastr, Devinity) {
    this.Restangular = Restangular;
    this.Devinity = Devinity;
    this.$q = $q;
    this.toastr = toastr;
    this.userFlags = [];
    this.flags = [];
    this.Restangular.all('flag').getList()
      .then(flags => {
        this.flags = this.Devinity.getGroupedFlags(flags);
      });
    this.initializing = true;
    var promise;
    var userId = parseInt(window.localStorage.getItem('userId'));
    if(userId) {
      promise = this.getOtherUserFlags(userId)
    } else {
      promise = this.getLoggedUserFlags()
    }
    promise
      .then(result => {
        this.flagSets = result.plain();
        this.userFlags = _.map(this.flagSets, flagSet => {
          var flag = flagSet.flag;
          flag.setId = flagSet.id;
          flagSet.flag = flag.id;
          return flag;
        });
        this.initializing = false;
      });
  };

  getLoggedUserFlags() {
    return this.Restangular.all('flags').getList()
  }

  getOtherUserFlags(userId) {
    return this.Restangular.all('flags').getList({user: userId})
  }

  saveFlags() {
    var prevFlagIds = _.map(this.userFlagsCopy, flag => flag.id);
    var currentFlagIds = _.map(this.userFlags, flag => flag.id);
    var toDelete = _.reject(prevFlagIds, id => _.includes(currentFlagIds, id));
    var toAdd = _.reject(currentFlagIds, id => _.includes(prevFlagIds, id));
    var promise = [];

    var flagSetsToDelete = _.filter(this.flagSets, flagSet => _.includes(toDelete, flagSet.flag));

    _.each(flagSetsToDelete, flagSet => {
      promise.push(this.Restangular.one('flags', flagSet.id).remove())
    });
    _.each(toAdd, id => {
      promise.push(this.Restangular.all('flags').post({flag: id}).then(res => {
        this.flagSets.push(res)
      }))
    });
    this.edit = false;
    this.toastr.success('', 'Health flags updated');
  }

  editFlags() {
    this.userFlagsCopy = angular.copy(this.userFlags);
    this.edit = true;
  };

  filterFlags(query) {
    return this.choices
  };

  isPositive(flag) {
    return flag ? flag.category == 1 : false;
  };

  isNegative(flag) {
    return flag ? flag.category == 3 : false;
  };

  isOpportunity(flag) {
    return flag ? flag.category == 2 : false;
  }

  getChoices() {
    if(this.userFlags.length < 4) {
      return _.reject(this.flags, flag => _.includes(_.map(this.userFlags, flag => flag.id), flag.id))
    } else {
      return []
    }
  };

  filterChoices($item, $model) {
    this.flags = _.reject(this.flags, flag => _.includes(_.map(this.flagSets, flagSet => flagSet.flag.id), flag.id))
  }

  showInfo(flag) {
    this.Devinity.showFlagInfo(flag);
  }

  static $inject = ['Restangular', '$q', 'toastr', 'Devinity']
}

export default FlagSectionController
