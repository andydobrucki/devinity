class SkillSectionController {
  constructor(Restangular, Devinity, toastr, $rootScope, $q, $scope) {
    this.choices = [];
    this.$scope = $scope;
    this.newAdded = null;
    this.edit = false;
    this.Restangular = Restangular;
    this.Devinity = Devinity;
    this.toastr = toastr;
    this.rootScope = $rootScope;
    this.skills = [];
    //this.toRemove =[];
    this.tags = [];
    this.q = $q;
    this.initializing = true;

    var userId = parseInt(window.localStorage.getItem('userId'));
    var query = {};

    if (userId) {
      query.user = userId
    }

    this.Restangular.all('skills/owned').getList(query)
      .then(result => {
        this.skills = _.sortByOrder(result.plain(), ['level'], ['desc']);
        this.tags = _.map(this.skills, skill => skill.tag);
        this.initializing = false;
      });
    this.levels = [
      {label: 'Apprentice', level: this.Devinity.levels.know},
      {label: 'Journeyman', level: this.Devinity.levels.canWorkWith},
      {label: 'Master', level: this.Devinity.levels.canTeach}
    ];
  };

  showInfo(skill) {
    this.Devinity.showOwnedSkillInfo(skill)
  };

  editSkills() {
    this.userFlagsCopy = angular.copy(this.skills);
    this.edit = true;
    this.addNew();
  }

  addNew() {
    this.new = true;
  }

  updateLevel($event) {
    let $value = $event.target.value.match(/.\.\d+/)[0] || null
    let value = $value ? parseFloat($value) : this.Devinity.levels.know
    this.newAdded.level = value
  }

  cancelNew() {
    this.new = false;
  }

  getChoices(search) {
    if(search && search.length > 0 ) {
      this.searching = true;
      this.Restangular.all('tag').getList({
        name: search,
        exclude: _.map(this.skills, skill => skill.tag.id)
      })
        .then(result => {
          this.searching = false;
          this.choices = _.map(result.plain(), choice => {
            choice.level = this.Devinity.levels.know;
            return choice;
          });
          this.noChoices = !Boolean(this.choices.length)
        })
    } else {
      this.choices = []
    }
  }

  saveSkills() {
    this.edit = false;
    this.saving = true;
    this.skills = _.sortByOrder(this.skills, ['level'], ['desc']);
    this.q.all(
      _.map(this.skills, skill => this.Restangular.one('skills', skill.id)
        .patch({level: skill.level}))
    ).then(result => {
      this.saving = false;
      this.toastr.success('', 'Skills updated');
    });
  }

  cancel() {
    this.skills = this.userFlagsCopy;
    this.edit = false;
  }

  saveNew() {
    this.new = false;
    this.saving = true;
    this.Restangular.all('skills').post({
      tag: this.newAdded.id,
      level: this.newAdded.level,
      familiar: this.Devinity.familiar.knows
    }).then(result => {
      var skill = result;
      skill.tag = this.newAdded;
      this.skills.push(skill);
      this.skills = _.sortByOrder(this.skills, ['level'], ['desc']);
      this.saving = false;
      this.new = true;
      this.rootScope.$broadcast('focusUiSelect', true);
      this.newAdded = null;
      this.toastr.success('', `Skill < ${skill.tag.name} > added`)
    });


  }

  remove(skill) {
    this.saving = true;
    $(`#skill_${skill.id}`).remove();
    this.skills = _.sortByOrder(_.filter(this.skills, el => el.id != skill.id), ['level'], ['desc']);
    this.userFlagsCopy = angular.copy(this.skills);
    this.Restangular.one('skills', skill.id).remove()
      .then(success => {
        this.saving = false;
        this.toastr.success('', `Skill < ${skill.tag.name} > removed`)
      })
  }


  removeSkill(skillId) {
    return this.Restangular.one('skills', skillId).remove();
  }

  updateSkillLevel({ id, level }) {
    return this.Restangular.one('skills', id).patch({level: level})
  }

  changeLevel(skill) {
    if(this.edit && !this.saving) {
      switch (skill.level) {
        case this.Devinity.levels.know:
          skill.level = this.Devinity.levels.canWorkWith;
          break;
        case this.Devinity.levels.canWorkWith:
          skill.level = this.Devinity.levels.canTeach;
          break;
        case this.Devinity.levels.canTeach:
          skill.level = this.Devinity.levels.know;
          break;
        default:
          skill.level = this.Devinity.levels.know;
      }
    }
  }

  static $inject = ['Restangular', 'Devinity', 'toastr', '$rootScope', '$q', '$scope']
}

export default SkillSectionController
