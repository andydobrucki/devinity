/**
 * Created by Wojciech Rydel <rydel.wojciech@gmail.com> on 22.11.15.
 */


class WantStuffRankingController {
  userCount;

  constructor(modal, Devinity, Restangular, $q) {
    this.modal = modal;
    this.Devinity = Devinity;
    this.Restangular = Restangular;
    this.ranking = [];
    this.q = $q;

    this.getRanking();
  }

  getRanking() {
    this.init = true;
    this.Restangular.all('skills/rank_wanted').getList()
      .then(ranking => {
        this.ranking = _.sortByOrder(ranking.plain(), ['count'], ['desc']);
        this.init = false;
      })
  }

  getPercent(skill) {
    return parseInt((skill.count / this.userCount) * 100);
  }

  showInfo(skill) {
    this.Devinity.showRankingSkillInfo(skill, true);
  }

  static $inject = ['$uibModal', 'Devinity', 'Restangular', '$q'];
}

export default WantStuffRankingController;
