/**
 * Created by wojciech on 30.10.15.
 */
const WANT_TO_LEARN = 0, KNOWS = 1, PASSIONATE = 2;

class Devinity {
  modalData = {
    controller: 'ModalInfoCtrl',
    bindToController: true,
    controllerAs: 'vm',
    size: 'lg',
    windowClass: 'slide-right',
    templateUrl: 'modalInfo.html'
  };

  levels = {
    canWorkWith: 0.66,
    know: 0.33, 
    canTeach: 0.95
  };

  flags = {
    opportunity: 1,
    information: 2,
    warning: 3
  };

  modalTypes = {wanted: 0, owned: 1, passionate: 2, flag: 3, ranking: 4};

  familiar = {
    wantToLearn: WANT_TO_LEARN,
    knows: KNOWS,
    passionate: PASSIONATE
  };

  importance = 0;

  constructor($uibModal, Restangular, $q) {
    this.modal = $uibModal;
    this.Restangular = Restangular;
    this.q = $q;
  };

  getDescription(tagId) {
    return this.Restangular.one('tag', tagId).customGET('wiki');
  }

  getSkillUsers(skillId) {
    return this.Restangular.one('skills', skillId).customGET('users')
  }

  getTagUsers(tagId) {
    return this.Restangular.one('tag', tagId).all('users').getList()
  }

  showModalInfo(skill, users, modalType) {
    this.modalData.resolve = {
      description() {
        return {text: skill.tag.description, title: skill.tag.name}
      },
      users() {
        return users
      },
      modalType() {
        return modalType
      },
      skillId() {
        return skill.id
      }
    };
    this.modal.open(this.modalData)
  };

  showRankingModalInfo(skill, users, modalType) {
    this.modalData.resolve = {
      description() {
        return {text: skill.tag.description, title: skill.tag.name}
      },
      users() {
        return users
      },
      modalType() {
        return modalType
      },
      skillId() {
        return skill.tag.id
      }
    };
    this.modal.open(this.modalData)
  }

  getGroupedFlags(flags) {
    return _.map(flags, flag => {
      switch (flag.category) {
        case this.flags.opportunity:
          flag.categoryName = 'Wants';
          break;
        case this.flags.information:
          flag.categoryName = 'Needs';
          break;
        case this.flags.warning:
          flag.categoryName = 'Alerts';
          break;
      };
      return flag;
    })
  };

  showFlagInfo(flag) {
    var vm = this;
        this.modalData.resolve = {
          description() {
            return {text: flag.description,  title: flag.name}
          },
          users() {
            return []
          },
          modalType() {
            return vm.modalTypes.flag;
          },
          skillId() {
            return flag.id
          }
        };
        this.modal.open(this.modalData)
  };

  showSkillInfo(skill, modalType) {
    if(!skill.tag.description) {
        this.getDescription(skill.tag.id)
          .then(result => {
            let wiki = result;
            skill.users = [];
            skill.tag.description = wiki.description;
            this.showModalInfo(skill, [], modalType);
          }, error => {
            skill.tag.description = null;
            skill.users = [];
            this.showModalInfo(skill, [], modalType)
          })
    } else {
      this.showModalInfo(skill, skill.users, modalType);
    }
  };

  showOwnedSkillInfo(skill) {
    this.showSkillInfo(skill, this.modalTypes.owned);
  };

  showPassionateSkillInfo(skill) {
    this.showSkillInfo(skill, this.modalTypes.passionate);
  };

  showWantedSkillInfo(skill) {
    this.showSkillInfo(skill, this.modalTypes.wanted);
  };

  showRankingTagModal(skill, toLearn=false) {
    let modalOptions = {
      controller: 'RankingTagModalCtrl',
      bindToController: true,
      controllerAs: 'vm',
      size: 'lg',
      windowClass: 'slide-right',
      templateUrl: 'modalInfo.html',
      resolve: {
        tagId: () => skill.tag.id,
        description: () => ({text: skill.tag.description, title: skill.tag.name}),
        toLearn: () => toLearn
      }
    };

    this.modal.open(modalOptions)
  }

  showRankingSkillInfo(skill, toLearn=false) {
    if (!skill.tag.description) {
      this.getDescription(skill.tag.id)
        .then(wiki => {
          skill.tag.description = wiki.description;
          skill.users = [];
          this.showRankingTagModal(skill, toLearn);
        }, error => {
            skill.tag.description = null;
            skill.users = [];
            this.showRankingTagModal(skill, toLearn)}
        )
    } else {
      this.showRankingModalInfo(skill, toLearn);
    }
  }

  static $inject = ['$uibModal', 'Restangular', '$q']
}

export {Devinity}
