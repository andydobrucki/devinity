class SkillRankingListDirective {
  scope = {
    userCount: '='
  };
  controller = 'SkillRankingCtrl';
  controllerAs = 'vm';
  bindToController = true;
  replace = true;
  templateUrl = 'skillRanking.html'
}

class WantStuffRankingDirective {
  scope = {
    userCount: '='
  };
  controller = 'WantedRankingCtrl';
  controllerAs = 'vm';
  bindToController = true;
  replace = true;
  templateUrl = 'wantsRanking.html';
}

class FlagsSelectDirective {
  templateUrl = 'flagsSelect.html';
  replace = true;
}

class ProfileSectionDirective {
  scope = {};
  controller = 'ProfileSectionCtrl';
  controllerAs = 'vm';
  templateUrl = 'profileSection.html';
  replace = true;
}

class LoveStuffSectionDirective {
  scope = {};
  controller = 'LoveStuffSectionCtrl';
  controllerAs = 'vm';
  templateUrl = 'loveStuffSection.html';
  replace = true;
}

class FlagSectionDirective {
  scope = {};
  controller = 'FlagSectionCtrl';
  controllerAs = 'vm';
  templateUrl = 'flagSection.html';
  replace = true;
}

class WantedStuffDirective {
  scope = {};
  controller = 'WantedStuffCtrl';
  controllerAs = 'vm';
  bindToController = true;
  replace = true;
  templateUrl = 'wantStuffSection.html'
}

class MatchingSectionDirective {
  scope = {};
  controller = 'MatchingSectionCtrl';
  controllerAs = 'vm';
  bindToController = true;
  replace = true;
  templateUrl = 'peerMatchingSection.html';
}

class SkillSectionDirective {
  scope = {};
  controller = 'SkillSectionCtrl';
  controllerAs = 'vm';
  replace = true;
  templateUrl = 'skillSection.html';
}

class SetFocus {
  constructor($timeout) {
    this.timeout = $timeout;
    this.link = ($scope, $element, $attr) => {
        $timeout(() => {$element.focus()})
    }
  }

  static factory($timeout) {
    return new SetFocus($timeout)
  }
}
SetFocus.factory.$inject = ['$timeout'];

class OnEnter {
  scope = {
    onEnter: '&'
  };

  link($scope, $element, $attr) {
    $element.on('keydown keypress', event => {
      if(event.keyCode == 13) {
        event.preventDefault();
        $scope.$apply(() => {$scope.onEnter()})
      }
    });
  }

}

class SearchSection {
  scope = {};
  controller = 'SearchSectionCtrl';
  controllerAs = 'vm';
  templateUrl = 'searchSection.html';
  replace = true;
}


class InfinityScroll {
  scope = {
    onEnd : '&',
    height: '='
  };

  link($scope, $element, $attrs) {
    var height = parseInt($scope.height);

    $element.css('max-height', `${height}px`);

    $element.on('scroll', function (event) {
      var $el = $(this);

      var scrollTop = $el.scrollTop();
      var innerHeight = $el.innerHeight();
      var scrollHeight = this.scrollHeight;

      if(scrollTop + innerHeight >= scrollHeight) {
          $scope.onEnd()
      }

    })
  }
}

class AccountManager {
  templateUrl = 'accountManager.html'
  replace = true
  scope = {
    userName: '=',
    avatar: '=',
    logoutUrl: '@'
  }

  link($scope, $element, $attrs) {
    $scope.$on('avatar:updated', (event, avatarUrl) => {
      console.log(avatarUrl)
      $scope.avatar = avatarUrl
      $scope.$applyAsync()
    })
    $scope.$on('devinity:user', (event, user) => {
      $scope.userName = user.name
      $scope.$applyAsync()
    })
  }
}

class Match {
  bindToController = true
  controller = 'PeerMatchCtrl'
  controllerAs = 'vm'
  replace = true
  templateUrl = 'peerMatch.html'
 
  scope = {
    peer: '=user'
  }
}


export {
  SkillRankingListDirective, WantStuffRankingDirective,
  FlagsSelectDirective, ProfileSectionDirective, LoveStuffSectionDirective,
  FlagSectionDirective, WantedStuffDirective, MatchingSectionDirective,
  SkillSectionDirective, SetFocus, OnEnter, SearchSection, InfinityScroll,
  AccountManager, Match
}
