from flask_restful import reqparse, abort, Resource
from flask_restful import fields
from flask_restful import marshal_with
from models import Skill
from database import session
import taglist


skill_fields = {
    'id': fields.Integer,
    #'user_id': fields.Nested(user_),
    'tag_id': fields.Nested(taglist.tag_fields),
    'familiar': fields.Integer,
    'level': fields.Float,
    'importance': fields.Integer,
}


def abort_if_change_doesnt_exist(change, id):
    if change is None:
        abort(404, message="Skill {} doesn't exist".format(id))


class SkillResource(Resource):
    @marshal_with(skill_fields)
    def get(self, id):
        skill = session.query(Skill).filter(Skill.id == id).first()
        abort_if_change_doesnt_exist(skill, id)
        return skill

#     def delete(self, change_id):
#         abort_if_change_doesnt_exist(change_id)
#         del changes[change_id]
#         return '', 204

#     def put(self, change_id):
#         args = parser.parse_args()
#         change = {'user': args['user']}
#         changes[change_id] = change
#         return change, 201


class SkillListResource(Resource):
    def get(self):
        return session.query(Skill).all()

    # def post(self):
    #     args = parser.parse_args()
    #     change_id = max(changes.keys()) + 1 if len(changes.keys()) > 0 else 1
    #     # change_id = 'change%i' % change_id
    #     changes[change_id] = {'user': args['user'], 'skill': args['skill'], "action": args["action"]}
    #     return changes[change_id], 201
