from flask import Flask, current_app, render_template, Response, \
    session, request, redirect
from flask_restful import reqparse, abort, Api, Resource
from flask.ext.login import LoginManager, login_user, logout_user, \
    login_required, current_user
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import Required
from flask.ext.principal import Principal, Permission, RoleNeed, Identity, \
    AnonymousIdentity, identity_changed
from flask_sqlalchemy import SQLAlchemy
from changelist import Change, ChangeList
from skilllist import SkillResource, SkillListResource
from matchlist import Match, MatchList
from taglist import TagResource, TagListResource
from rubicore import Mjolnir, MjolnirAll
import sys
import database

# Initialize Flask app singleton
app = Flask(__name__)

# Load the extensions

# Alchemy
db = SQLAlchemy(app)
# Principal
principals = Principal(app)
# Restful API
api = Api(app)

admin_permission = Permission(RoleNeed('admin'))
login_manager = LoginManager(app)


# Must have main app methods
def init_db():
    print "Initiating SQL Alchemy..."
    db.create_all()
    print "Initiating Rubicore SQL Alchemy..."
    database.init_db()
    print "Rubicore Alchemy initialized"


# protect a view with a principal for that need
@app.route('/admin')
@admin_permission.require()
def do_admin_index():
    return Response('Only if you are an admin')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@login_manager.user_loader
def load_user(userid):
    # Return an instance of the User model
    return datastore.find_user(id=userid)


class HelloStranger(Resource):
    def get(self):
        # flash('Hello Stranger')
        return {'hello': 'world'}


class LoginForm(Form):
    email = TextField()
    password = PasswordField()


@app.route('/')
def welcome():
    return render_template('index.html')  # render a template


@app.route('/login', methods=['GET', 'POST'])
def login():
    # A hypothetical login form that uses Flask-WTF
    form = LoginForm()

    # Validate form input
    if form.validate_on_submit():
        # Retrieve the user from the hypothetical datastore
        user = datastore.find_user(email=form.email.data)

        # Compare passwords (use password hashing production)
        if form.password.data == user.password:
            # Keep the user info in the session using Flask-Login
            login_user(user)

            # Tell Flask-Principal the identity changed
            identity_changed.send(current_app._get_current_object(),
                                  identity=Identity(user.id))

            return redirect(request.args.get('next') or '/')

    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    # Remove the user information from the session
    logout_user()

    # Remove session keys set by Flask-Principal
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(current_app._get_current_object(),
                          identity=AnonymousIdentity())

    return redirect(request.args.get('next') or '/')


api.add_resource(HelloStranger, '/hello')
## API resource routing
api.add_resource(MjolnirAll, '/calculate')
api.add_resource(Mjolnir, '/calculate/<string:organization_id>')

api.add_resource(ChangeList, '/changes', endpoint='changes')
api.add_resource(Change, '/changes/<string:id>', endpoint='change')

api.add_resource(SkillListResource, '/skills', endpoint='skills')
api.add_resource(SkillResource, '/skills/<string:id>', endpoint='skill')

api.add_resource(TagListResource, '/tags', endpoint='tags')
api.add_resource(TagResource, '/tags/<string:id>', endpoint='tag')

api.add_resource(MatchList, '/matches', endpoint='matches')
api.add_resource(Match, '/matches/<string:id>', endpoint='match')


if __name__ == '__main__':
    print "Starting Rubicore management app..."
    init_db()
    debug_mode = sys.argv[1] in ['True', 'Y', 'yes', 'true']
    app.run(host='0.0.0.0', port=5001, debug=debug_mode)
    print "Application Rubicore running, debug set to " + str(debug_mode)
