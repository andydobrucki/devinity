from flask_restful import reqparse, abort, Resource
from models import Change
from database import session


# parser = reqparse.RequestParser()
# parser.add_argument('user', required=True)
# parser.add_argument('skill', required=True)
# parser.add_argument('action', required=True)


# def abort_if_change_doesnt_exist(change_id):
#     if change_id not in changes:
#         abort(404, message="Change {} doesn't exist".format(change_id))


class Change(Resource):
    def get(self, change_id):
        #abort_if_change_doesnt_exist(change_id)
        return session.query(Change).get(change_id)

#     def delete(self, change_id):
#         abort_if_change_doesnt_exist(change_id)
#         del changes[change_id]
#         return '', 204

#     def put(self, change_id):
#         args = parser.parse_args()
#         change = {'user': args['user']}
#         changes[change_id] = change
#         return change, 201


class ChangeList(Resource):
    def get(self):
        return session.query(Change).all()

    # def post(self):
    #     args = parser.parse_args()
    #     change_id = max(changes.keys()) + 1 if len(changes.keys()) > 0 else 1
    #     # change_id = 'change%i' % change_id
    #     changes[change_id] = {'user': args['user'], 'skill': args['skill'], "action": args["action"]}
    #     return changes[change_id], 201
