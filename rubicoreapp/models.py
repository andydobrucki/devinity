from sqlalchemy import Column, Integer, String, Numeric, DateTime, ForeignKey
from database import Base


class Change(Base):
    __tablename__ = 'rubicoreapp_incoming'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    tag_id = Column(Integer)
    familiar = Column(Integer)
    level = Column(Numeric)
    importance = Column(Integer)
    created_at = Column(DateTime)
    action = Column(Integer, ForeignKey("changetype.id"), nullable=False),

    def __init__(self, user_id=None, tag_id=None, familiar=None, level=None, importance=None, created_at=None, action=None):
        self.user_id = user_id
        self.tag_id = tag_id
        self.familiar = familiar
        self.level = level
        self.importance = importance
        self.created_at = created_at
        self.action = action

    def __repr__(self):
        return '<Change %r>' % (self.id)


class ChangeType(Base):
    __tablename__ = 'rubicoreapp_incoming_action'
    id = Column(Integer, primary_key=True)
    name = Column(String(24))

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<ChangeType is %r>' % (self.name)


# Niezbedne klasy User, Skill, Tag, to wsio
class User(Base):
    __tablename__ = 'rubicoreapp_user'
    id = Column(Integer, primary_key=True)
    login = Column(String(50))
    name = Column(String(50))
    email = Column(String(254))
    position = Column(String(50))
    organization_id = Column(Integer)
    joined = Column(DateTime)

    def __init__(self, login=None, name=None, email=None, position=None, organization_id=None, joined=None, action=None):
        self.login = login
        self.name = name
        self.email = email
        self.position = position
        self.organization_id = organization_id
        self.joined = joined

    def __repr__(self):
        return '<User %r>' % (self.login)


class Skill(Base):
    __tablename__ = 'rubicoreapp_skill'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False),
    tag_id = Column(Integer, ForeignKey("tag.id"), nullable=False),
    familiar = Column(Integer)
    level = Column(Numeric, nullable=True)
    importance = Column(Integer, nullable=True)

    def __init__(self, user_id=None, tag_id=None, familiar=None, level=None, importance=None):
        self.user_id = user_id
        self.tag_id = tag_id
        self.familiar = familiar
        self.level = level
        self.importance = importance

    def __repr__(self):
        return '<Skill %r, owner %r>' % (self.tag_id, self.user_id)


class Tag(Base):
    __tablename__ = 'rubicoreapp_tag'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime),
    count = Column(Integer),
    name = Column(String(50))
    wiki_id = Column(Integer)

    def __init__(self, timestamp=None, count=None, name=None, wiki_id=None):
        self.timestamp = timestamp
        self.count = count
        self.name = name
        self.wiki_id = wiki_id

    def __repr__(self):
        return '<Tag %r>' % (self.name)
