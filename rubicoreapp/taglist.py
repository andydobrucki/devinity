from flask_restful import reqparse, abort, Resource
from flask_restful import fields
from flask_restful import marshal_with
from models import Tag
from database import session


# parser = reqparse.RequestParser()
# parser.add_argument('user', required=True)
# parser.add_argument('skill', required=True)
# parser.add_argument('action', required=True)

tag_fields = {
    'id': fields.Integer,
    'timestamp': fields.DateTime,
    'count': fields.Integer,
    'name': fields.String,
    'wiki_id': fields.Integer,
}



def abort_if_change_doesnt_exist(tag, id):
    if tag is None:
        abort(404, message="Tag {} doesn't exist".format(id))


class TagResource(Resource):
    @marshal_with(tag_fields)
    def get(self, id):
        tag = session.query(Tag).filter(Tag.id == id).first()
        abort_if_change_doesnt_exist(tag, id)
        return tag

#     def delete(self, change_id):
#         abort_if_change_doesnt_exist(change_id)
#         del changes[change_id]
#         return '', 204

#     def put(self, change_id):
#         args = parser.parse_args()
#         change = {'user': args['user']}
#         changes[change_id] = change
#         return change, 201


class TagListResource(Resource):
    def get(self):
        return session.query(Tag).all()

    # def post(self):
    #     args = parser.parse_args()
    #     change_id = max(changes.keys()) + 1 if len(changes.keys()) > 0 else 1
    #     # change_id = 'change%i' % change_id
    #     changes[change_id] = {'user': args['user'], 'skill': args['skill'], "action": args["action"]}
    #     return changes[change_id], 201
